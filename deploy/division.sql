-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for crewlink_db
CREATE DATABASE IF NOT EXISTS `crewlink_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `crewlink_db`;

-- Dumping structure for table crewlink_db.division
CREATE TABLE IF NOT EXISTS `division` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `mark_delete` bit(1) DEFAULT b'0',
  `name` varchar(255) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

-- Dumping data for table crewlink_db.division: ~69 rows (approximately)
/*!40000 ALTER TABLE `division` DISABLE KEYS */;
INSERT INTO `division` (`id`, `created_at`, `mark_delete`, `name`, `updated_at`) VALUES
	(1, NULL, b'0', 'BB', NULL),
	(2, NULL, b'0', 'BSL', NULL),
	(3, NULL, b'0', 'NGP', NULL),
	(4, NULL, b'0', 'PUNE', NULL),
	(5, NULL, b'0', 'SUR', NULL),
	(6, NULL, b'0', 'KUR', NULL),
	(7, NULL, b'0', 'SBP', NULL),
	(8, NULL, b'0', 'WAT', NULL),
	(9, NULL, b'0', 'DNR', NULL),
	(10, NULL, b'0', 'MGS', NULL),
	(11, NULL, b'0', 'DHN', NULL),
	(12, NULL, b'0', 'SEE', NULL),
	(13, NULL, b'0', 'SPJ', NULL),
	(14, NULL, b'0', 'ASN', NULL),
	(15, NULL, b'0', 'HWH', NULL),
	(16, NULL, b'0', 'MLDT', NULL),
	(17, NULL, b'0', 'SDAH', NULL),
	(18, NULL, b'0', 'KAWR', NULL),
	(19, NULL, b'0', 'RN', NULL),
	(20, NULL, b'0', 'AGRA', NULL),
	(21, NULL, b'0', 'ALD', NULL),
	(22, NULL, b'0', 'JHS', NULL),
	(23, NULL, b'0', 'BSB', NULL),
	(24, NULL, b'0', 'LJN', NULL),
	(25, NULL, b'0', 'IZN', NULL),
	(26, NULL, b'0', 'APDJ', NULL),
	(27, NULL, b'0', 'KIR', NULL),
	(28, NULL, b'0', 'LMG', NULL),
	(29, NULL, b'0', 'RNY', NULL),
	(30, NULL, b'0', 'DLI', NULL),
	(31, NULL, b'0', 'FZR', NULL),
	(32, NULL, b'0', 'LKO', NULL),
	(33, NULL, b'0', 'MB', NULL),
	(34, NULL, b'0', 'UMB', NULL),
	(35, NULL, b'0', 'AII', NULL),
	(36, NULL, b'0', 'BKN', NULL),
	(37, NULL, b'0', 'JP', NULL),
	(38, NULL, b'0', 'JU', NULL),
	(39, NULL, b'0', 'BZA', NULL),
	(40, NULL, b'0', 'GNT', NULL),
	(41, NULL, b'0', 'GTL', NULL),
	(42, NULL, b'0', 'HYB', NULL),
	(43, NULL, b'0', 'NED', NULL),
	(44, NULL, b'0', 'SC', NULL),
	(45, NULL, b'0', 'BSP', NULL),
	(46, NULL, b'0', 'NAG', NULL),
	(47, NULL, b'0', 'R', NULL),
	(48, NULL, b'0', 'ADRA', NULL),
	(49, NULL, b'0', 'CKP', NULL),
	(50, NULL, b'0', 'KGP', NULL),
	(51, NULL, b'0', 'RNC', NULL),
	(52, NULL, b'0', 'MAS', NULL),
	(53, NULL, b'0', 'MDU', NULL),
	(54, NULL, b'0', 'PGT', NULL),
	(55, NULL, b'0', 'SA', NULL),
	(56, NULL, b'0', 'TPJ', NULL),
	(57, NULL, b'0', 'TVC', NULL),
	(58, NULL, b'0', 'MYS', NULL),
	(59, NULL, b'0', 'SBC', NULL),
	(60, NULL, b'0', 'UBL', NULL),
	(61, NULL, b'0', 'BPL', NULL),
	(62, NULL, b'0', 'JBP', NULL),
	(63, NULL, b'0', 'KOTA', NULL),
	(64, NULL, b'0', 'ADI', NULL),
	(65, NULL, b'0', 'BCT', NULL),
	(66, NULL, b'0', 'BRC', NULL),
	(67, NULL, b'0', 'BVC', NULL),
	(68, NULL, b'0', 'RJT', NULL),
	(69, NULL, b'0', 'RTM', NULL);
/*!40000 ALTER TABLE `division` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
