-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for crewlink_db
CREATE DATABASE IF NOT EXISTS `crewlink_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `crewlink_db`;

-- Dumping structure for table crewlink_db.train_type
CREATE TABLE IF NOT EXISTS `train_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `mark_delete` bit(1) DEFAULT b'0',
  `name` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Dumping data for table crewlink_db.train_type: ~17 rows (approximately)
/*!40000 ALTER TABLE `train_type` DISABLE KEYS */;
INSERT INTO `train_type` (`id`, `created_at`, `created_time`, `mark_delete`, `name`, `updated_at`) VALUES
	(1, '2022-02-11 22:48:26', '2022-02-11 22:48:26', b'0', 'GBR', '2022-02-11 22:48:26'),
	(18, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '         DMU   ', '2022-02-12 04:35:15'),
	(19, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '         DRNT   ', '2022-02-12 04:35:15'),
	(20, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        EMU    ', '2022-02-12 04:35:15'),
	(21, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        GBR    ', '2022-02-12 04:35:15'),
	(22, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        HSP    ', '2022-02-12 04:35:15'),
	(23, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        JSH    ', '2022-02-12 04:35:15'),
	(24, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        MEMU   ', '2022-02-12 04:35:15'),
	(25, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        MEX    ', '2022-02-12 04:35:15'),
	(26, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        PAS     ', '2022-02-12 04:35:15'),
	(27, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        PRUM    ', '2022-02-12 04:35:15'),
	(28, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        RAJ     ', '2022-02-12 04:35:15'),
	(29, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        RBUS    ', '2022-02-12 04:35:15'),
	(30, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        SHT     ', '2022-02-12 04:35:15'),
	(31, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        SUB     ', '2022-02-12 04:35:15'),
	(32, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        SUF    ', '2022-02-12 04:35:15'),
	(33, '2022-02-12 04:35:15', '2022-02-12 04:35:15', b'0', '        SUVD    ', '2022-02-12 04:35:15');
/*!40000 ALTER TABLE `train_type` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
