--
--SELECT r.name ,r.description, pp.can_create_users, pp.can_delete_users
--FROM role AS r
--INNER JOIN role_premissions AS rp ON (r.id = rp.roles_id)
--INNER JOIN premission AS pp ON (pp.id = rp.premissions_id)




SELECT role.rolename as name,premission.can_create_users,premission.can_delete_users FROM role
JOIN role_premissions ON (role.id = role_premissions.roles_id)
JOIN premission ON (premission.id = role_premissions.premissions_id)