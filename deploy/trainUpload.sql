-- Dumping structure for procedure crewlink_db.CreateTrain
DROP PROCEDURE IF EXISTS `CreateTrain`;
DELIMITER //
CREATE DEFINER=`crewlink_db`@`localhost` PROCEDURE `CreateTrain`(IN `trainNo` INT, IN `name` VARCHAR(255), IN `fromStation` VARCHAR(255), IN `toStation` VARCHAR(255), IN `startDay` VARCHAR(255), IN `trainType` VARCHAR(255), OUT `createdTrainId` BIGINT, OUT `result` TINYINT, OUT `errorMessage` VARCHAR(255))
BEGIN
DECLARE fsid BIGINT DEFAULT 0;
DECLARE tsid BIGINT DEFAULT 0;
DECLARE trainId BIGINT DEFAULT 0;
DECLARE ttid BIGINT DEFAULT 0;
DECLARE startDayVal BIGINT;
DECLARE trainIdExist BIGINT;
DECLARE done int;

DECLARE curs1 CURSOR FOR
        SELECT  value FROM splitStringTempTable;

DECLARE curs CURSOR FOR
SELECT t.id
		FROM train as t
	   WHERE t.train_no = trainNo
              AND NOT FIND_IN_SET(t.start_day, startDay);

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

DECLARE exit handler for sqlexception
  BEGIN
    SET result = 0;
    SET errorMessage = 'Error occured in Creating Train';
  ROLLBACK;
END;

SET result = 1;
CALL SplitStringToTempTable(startDay, NULL, NULL, ',');

START TRANSACTION;
    SET @fs = fromStation;
	SET @ts = toStation;
    SET @tt= trainType;
    SET @fsidcount = 0;
    SET @tsidcount = 0; 
    SET @ttcount = 0;
    SET @trainidcount=0;
    SET @trainNo=trainNo;
    SET @trainName=name;
    SET @startDay=startDay;

    SET @isTrainIdExist=0;
    SET @dbTrainStartDay=0;
    
    
  OPEN curs;
      curs_loop : LOOP
				FETCH curs INTO trainIdExist; 

        DELETE from train  WHERE id=trainIdExist;
				IF done = 1 THEN
					LEAVE curs_loop;
				END IF;
     END LOOP;
		CLOSE curs;

 SET done=0;

  OPEN curs1;

			curs_loop : LOOP
				FETCH curs1 INTO startDayVal; 

				IF done = 1 THEN
					LEAVE curs_loop;
				END IF;

       
   
		SELECT COUNT(s.id) into @fsidcount FROM 
							  station AS s
							  WHERE s.code = @fs;
		

		IF (@fsidcount != 1) THEN
				INSERT INTO station (code,head_station_sign_off_duration,head_station_sign_on_duration,name,number_of_beds,out_station_sign_off_duration,out_station_sign_on_duration)VALUES(@fs,30,30,'',1,30,30);
				SET fsid = LAST_INSERT_ID();
		ELSE
		        UPDATE station s SET s.head_station_sign_off_duration=30,
		                             s.head_station_sign_on_duration = 30,
		                             s.number_of_beds=1,
		                             s.out_station_sign_off_duration=30,
		                             s.out_station_sign_on_duration=30
		        WHERE s.code=@fs;
            SELECT s.id into fsid FROM 
							  station AS s
							  WHERE s.code = @fs;
		      
		END IF;
	
	
		SELECT COUNT(s.id) into @tsidcount FROM 
							  station AS s
							  WHERE s.code = @ts;
	
		IF (@tsidcount != 1) THEN
				INSERT INTO station (code,head_station_sign_off_duration,head_station_sign_on_duration,name,number_of_beds,out_station_sign_off_duration,out_station_sign_on_duration)VALUES(toStation,30,30,'',1,30,30);
				SET tsid = LAST_INSERT_ID();
		ELSE
		        UPDATE station s SET s.head_station_sign_off_duration=30,
		                             s.head_station_sign_on_duration = 30,
		                             s.number_of_beds=1,
		                             s.out_station_sign_off_duration=30,
		                             s.out_station_sign_on_duration=30
		        WHERE s.code=@ts;
		        
            SELECT s.id into tsid FROM 
							  station AS s
							  WHERE s.code = @ts;
		END IF;
		
		SELECT COUNT(tt.id) into @ttcount
		FROM train_type as tt
		WHERE tt.name = @tt;
		
		IF (@ttcount != 1) THEN
				INSERT INTO train_type(name) VALUES(trainType);
				SET ttid = LAST_INSERT_ID();
    ELSE
        SELECT tt.id into ttid
  		  FROM train_type as tt
  		  WHERE tt.name = @tt;
		
		END IF;
	
		
		SELECT COUNT(t.id) into @trainidcount 
		FROM train as t
		WHERE t.train_no=@trainNo and t.start_day=startDayVal;
			
		IF (@trainidcount != 1) THEN
					
					INSERT INTO train(name,start_day,train_no,from_station,to_station,train_type) VALUES(@trainName,startDayVal,@trainNo,fsid,tsid,ttid);
			SET createdTrainId = LAST_INSERT_ID();
		ELSE
				
				UPDATE train SET name=@trainName,
							        from_station=fsid,
							        to_station=tsid,
							        train_type=ttid
				WHERE train_no=@trainNo and start_day=startDayVal;

        SELECT t.id INTO createdTrainId FROM train AS t WHERE train_no=@trainNo and start_day=startDayVal;
	END IF;

    END LOOP;
		CLOSE curs1;

IF ( result = 1 ) THEN
	SET result = 1;
	SET errorMessage = 'SUCCESS';
	COMMIT;
ELSE
	ROLLBACK;
END IF;
END//
DELIMITER ;