-- Dumping structure for procedure crew link_db.CreateTrainStations
DROP PROCEDURE IF EXISTS `CreateTrainStations`;
DELIMITER //
CREATE DEFINER=`crewlink_db`@`localhost` PROCEDURE `CreateTrainStations`(IN `trainNo` INT, IN `stopNumber` INT, IN `stationCode` VARCHAR(255), IN `dayOfJourney` INT, IN `arrivalTime` VARCHAR(255), IN `departureTime` VARCHAR(255), IN `distance` BIGINT, OUT `result` TINYINT, OUT `errorMessage` VARCHAR(255))
    COMMENT 'This will create or update Train Stations'
BEGIN
DECLARE t_id BIGINT;
DECLARE i int DEFAULT 0;
DECLARE startDay VARCHAR(255);

DECLARE done BOOLEAN DEFAULT FALSE;
DECLARE curs CURSOR FOR
        SELECT t.id,t.start_day FROM train as t WHERE t.train_no = trainNo;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE exit handler for sqlexception
BEGIN
	SET result = 0;
	SET errorMessage = 'Error Occurred in Upload Train_running_details';
	ROLLBACK;
END;

SET @trainCount = 0;
SELECT COUNT(t.id) INTO @trainCount  FROM train as t WHERE t.train_no = trainNo;

OPEN curs;
        SET done = FALSE;
        curs_loop: LOOP
        FETCH curs INTO t_id,startDay;
        IF i = @trainCount THEN
            LEAVE curs_loop;
         END IF;
        

START TRANSACTION;
        SET @ts_id = NULL;
        SELECT ts.id into @ts_id FROM train_station as ts WHERE ts.train = t_id AND ts.stop_number = stopNumber;
        SET @station = NULL;
        SELECT s.id into @station FROM station as s WHERE s.code = stationCode;
        IF( @station IS NULL ) THEN
             
          INSERT INTO station (code,head_station_sign_off_duration,head_station_sign_on_duration,name,number_of_beds,out_station_sign_off_duration,out_station_sign_on_duration)VALUES(stationCode,30,30,'',1,30,30);

        END IF;
        IF @ts_id IS NOT NULL THEN
          SET @ts1Dep = NULL;
          SET @ts1Day = NULL;
          SET @ts1Arr = NULL;
          SET @ts1JD = NULL;
          SELECT ts1.departure,ts1.day,ts1.arrival, ts1.journey_duration INTO @ts1Dep,@ts1Day, @ts1Arr, @ts1JD FROM train_station as ts1 WHERE ts1.train = t_id AND ts1.stop_number = (stopNumber-1);
          IF(@ts1Dep IS NULL) THEN
            
            UPDATE train_station  SET arrival = arrivalTime,
                                       departure = departureTime,
                                       day_of_journey= dayOfJourney,
                                       distance = distance,
                                       station = @station,
                                       stop_number= stopNumber,
                                       train = t_id,
                                       journey_duration = 0,
                                       day = (startDay + dayOfJourney -1) % 7
                                       WHERE id = @ts_id;
          ELSE
          	SET @ts1ADay= @ts1Day;
          	SET @ts1DDay= @ts1Day;
            IF( TIMEDIFF(CAST(@ts1Dep as TIME), CAST(@ts1Arr as TIME)) <0 ) THEN
              SET @ts1Day = (@ts1Day +1) % 7;
              SET @ts1DDay = @ts1Day;
            END IF;
            SET @ts2Day = (startDay + dayOfJourney - 1) % 7;
            SET @ts2JD = ((TIME_TO_SEC(TIMEDIFF(CAST(arrivalTime as TIME), CAST(@ts1Dep as TIME)))/60) +(IF(@ts2Day - @ts1Day < 0 , 7 - (@ts1Day - @ts2Day), (@ts2Day - @ts1Day))*1440)) + @ts1JD;
            IF( @ts1Arr NOT LIKE '%0:00:00%' ) THEN
					SET @ts2JD = @ts2JD + ((TIME_TO_SEC(TIMEDIFF(CAST(@ts1Dep as TIME), CAST(@ts1Arr as TIME)))/60) +(IF(@ts1DDay - @ts1ADay < 0 , 7 - (@ts1ADay - @ts1DDay), (@ts1DDay - @ts1ADay))*1440));
				END IF;
				UPDATE train_station SET arrival = arrivalTime,
                                       departure = departureTime,
                                       day_of_journey= dayOfJourney,
                                       distance = distance,
                                       station = @station,
                                       stop_number= stopNumber,
                                       train = t_id,
                                       journey_duration = @ts2JD,
                                       day = @ts2Day
                                       WHERE id = @ts_id;

          END IF;
        ELSE
          
          SET @ts1Dep = NULL;
          SET @ts1Day = NULL;
          SET @ts1Arr = NULL;
          SET @ts1JD = NULL;
          SELECT ts1.departure,ts1.day,ts1.arrival, ts1.journey_duration INTO @ts1Dep,@ts1Day, @ts1Arr, @ts1JD FROM train_station as ts1 WHERE ts1.train = t_id AND ts1.stop_number = (stopNumber-1);
          IF(@ts1Dep IS NULL) THEN
            INSERT INTO train_station (arrival,
                                       day,
                                       day_of_journey,
                                       departure,
                                       distance,
                                       journey_duration,
                                       stop_number,
                                       station,
                                       train)VALUES(arrivalTime,
                                                    (startDay + dayOfJourney -1) % 7,
                                                    dayOfJourney,
                                                    departureTime,
                                                    distance,
                                                    0,
                                                    stopNumber,
                                                    @station,
                                                    t_id);
          ELSE
            SET @ts1ADay= @ts1Day;
          	SET @ts1DDay= @ts1Day;
            IF( TIMEDIFF(CAST(@ts1Dep as TIME), CAST(@ts1Arr as TIME)) <0 ) THEN
              SET @ts1Day = (@ts1Day +1) % 7;
              SET @ts1DDay = @ts1Day;
            END IF;
            SET @ts2Day = (startDay + dayOfJourney - 1) % 7;
            SET @ts2JD = ((TIME_TO_SEC(TIMEDIFF(  CAST(arrivalTime as TIME), CAST(@ts1Dep as TIME)))/60) +(IF(@ts2Day - @ts1Day < 0 , 7 - (@ts1Day - @ts2Day), (@ts2Day - @ts1Day))*1440)) + @ts1JD;
            IF( @ts1Arr NOT LIKE '%0:00:00%' ) THEN
            	SET @ts2JD = @ts2JD + ((TIME_TO_SEC(TIMEDIFF(CAST(@ts1Dep as TIME), CAST(@ts1Arr as TIME)))/60) +(IF(@ts1DDay - @ts1ADay < 0 , 7 - (@ts1ADay - @ts1DDay), (@ts1DDay - @ts1ADay))*1440));
				END IF;
				INSERT INTO train_station (arrival,
                                      day,
                                      day_of_journey,
                                      departure,
                                      distance,
                                      journey_duration,
                                      stop_number,
                                      station,
                                      train)VALUES(arrivalTime,
                                                   @ts2Day,
                                                   dayOfJourney,
                                                   departureTime,
                                                   distance,
                                                   @ts2JD,
                                                   stopNumber,
                                                   @station,
                                                   t_id);


          END IF;
        END IF;
       
    
    SET result = 1;
	  SET errorMessage = 'SUCCESS';
  COMMIT;
  SET i = i + 1;
  END LOOP;
CLOSE curs;
  
END//
DELIMITER ;