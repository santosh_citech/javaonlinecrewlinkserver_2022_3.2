BEGIN
     SET @SORT = "name";
     SET @SIZE = 10;
     SET @PAGE = 0;
     SET @result = 0;
     SET @message = "";
    call stationRecordProcedure(@SORT,@SIZE,@PAGE,@message,@result,@recordList);
    SELECT @message;
    SELECT @result;
    SELECT @recordList;
END


BEGIN
    SET @TrainNo = 12594;
    SET @TrainName = 'BPL-LJN-EXP';
    SET @fromStation ='BPL';
    SET @toStation ='LJN';
    SET @startDay = 'y' ;
    SET @trainType= 'GBR';
    SET @createdTrainId = 0;
    SET @result = 0;
    SET @errorMessage = '';

    call CreateTrainProcedure(@TrainNo,@TrainName,@fromStation,@toStation,@startDay,@trainType,@createdTrainId,@result,@errorMessage);
    SELECT @result;
    SELECT @CreatedId;
    SELECT @errorMessage;


    
    SET @createdTrainId = 0;
    SET @result = 0;
    SET @errormessage = '';

    call TrainUpload(12594,'BPL-LJN-EXP','BPL','LJN','0,1,2,3,4,5,6','GBR',@createdTrainId,@result,@errormessage);
    SELECT @result;
    SELECT @createdTrainId;
    SELECT @errormessage;


END


BEGIN

DECLARE startDayVal BIGINT;
DECLARE done int;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
         DECLARE curs1 CURSOR FOR
        SELECT  value FROM runningDaysTempTable;

           call RunningDays('0,1,2,3,4,5,6',',');



  OPEN curs1;

			curs_loop : LOOP
				FETCH curs1 INTO startDayVal;

				IF done = 1 THEN
					LEAVE curs_loop;
				END IF;

         END LOOP;
		CLOSE curs1;
END




BEGIN

call TrainStationUpload(12594,1,'bpl',1,'0:00','22:45',0);
call TrainStationUpload(12594,2,'bhs',1,'23:22','23:24',53);
call TrainStationUpload(12594,3,'bina',2,'1:00','1:05',138);
call TrainStationUpload(12594,4,'jhs',2,'3:15','3:25',289);
call TrainStationUpload(12594,5,'cnb',2,'8:15','8:20',508);
call TrainStationUpload(12594,6,'ljn',2,'9:15','0:00',582);



    SET @TrainNo = 10103;
    SET @CODE = 'CSTM';
    SET @STOP = 1;
    SET @ARRIVAL ='0:00:00';
    SET @DEPARTURE ='07:10:00';
    SET @Day = 1 ;
    SET @DISTANCE = 0;
    SET @errorMessage = '';

call TrainStationUpload(@TrainNo,@STOP,@CODE,@DAY,@ARRIVAL,@DEPARTURE,@DISTANCE,@errorMessage);
call TrainStationUpload(10103,2,'DR',1,'07:22:00','07:25:00',53,@errorMessage);
call TrainStationUpload(10103,3,'TNA',2,'07:47:00','07:50:00',138,@errorMessage);
call TrainStationUpload(10103,4,'PNVL',2,'08:25:00','08:30:00',289,@errorMessage);
SELECT @errorMessage;
-- call TrainStationUpload(10103,5,'cnb',2,'8:15','8:20',508);
-- call TrainStationUpload(12594,6,'ljn',2,'9:15','0:00',582);

END