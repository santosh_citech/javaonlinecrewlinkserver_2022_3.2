package com.mathologic.projects.crewlink.utility.library;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author : ADMIN 2/16/2022
 * @created : 16 / 02 / 2022 - 3:21 AM
 **/
class TimeUtilityTest {

    TimeUtility timeUtility;
    DateTimeOBJ dateTimeOBJ;

    @BeforeEach
    void setUp() {
        timeUtility = new TimeUtility();
        dateTimeOBJ = new DateTimeOBJ();
        dateTimeOBJ.day = 1;
        dateTimeOBJ.time="7:00:00";
        dateTimeOBJ.days=1;
    }

    @Test
    @DisplayName("Simple multiplication should work")
    void testMultiply() {
        assertEquals(1860, timeUtility.convertDateTimeObjToNumber(dateTimeOBJ, "mins"),
                "Regular multiplication should work");
    }

}