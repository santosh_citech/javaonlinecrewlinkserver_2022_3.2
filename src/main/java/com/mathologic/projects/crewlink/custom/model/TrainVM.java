package com.mathologic.projects.crewlink.custom.model;

import java.io.Serializable;

/**
 * @author : ADMIN 2/19/2022
 * @created : 19 / 02 / 2022 - 7:37 PM
 **/
public class TrainVM implements Serializable {


    private Integer trainNo;
    private String name;
    private String fromStation;
    private String toStation;
    private String trainType;
    private Integer runningDay;

    public TrainVM(Integer trainNo, String name, String fromStation, String toStation, String trainType, Integer runningDay) {

        this.trainNo = trainNo;
        this.name = name;
        this.fromStation = fromStation;
        this.toStation = toStation;
        this.trainType = trainType;
        this.runningDay = runningDay;
    }



    public Integer getTrainNo() {
        return trainNo;
    }

    public void setTrainNo(Integer trainNo) {
        this.trainNo = trainNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFromStation() {
        return fromStation;
    }

    public void setFromStation(String fromStation) {
        this.fromStation = fromStation;
    }

    public String getToStation() {
        return toStation;
    }

    public void setToStation(String toStation) {
        this.toStation = toStation;
    }

    public String getTrainType() {
        return trainType;
    }

    public void setTrainType(String trainType) {
        this.trainType = trainType;
    }

    public Integer getRunningDay() {
        return runningDay;
    }

    public void setRunningDay(Integer runningDay) {
        this.runningDay = runningDay;
    }
}
