package com.mathologic.projects.crewlink.custom.repository;

import java.util.List;

public interface UserVMRepository {


    @SuppressWarnings("rawtypes")
    List getTotalUsedDistance(Long userPlan, String station);

    @SuppressWarnings("rawtypes")
    List getTotalAvailableDistanceFrom(Long userPlan, String station);

    @SuppressWarnings("rawtypes")
    List getTotalAvailableDistanceTo(Long userPlan, String station);

    @SuppressWarnings("rawtypes")
    List getTotalAvailableDistanceBoth(Long userPlan, String station);

    List getAllUser(String sort, Long page, Long size);
}
