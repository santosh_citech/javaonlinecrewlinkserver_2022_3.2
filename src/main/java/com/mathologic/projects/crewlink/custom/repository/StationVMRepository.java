package com.mathologic.projects.crewlink.custom.repository;

import com.mathologic.projects.crewlink.custom.model.StoredProcedureProcessResult;

import java.util.List;

public interface StationVMRepository {
   List<?> getAllStations(String sort, Long page, Long size);
   StoredProcedureProcessResult getStationsByProcedure(String sort, Long page, Long size);
}
