package com.mathologic.projects.crewlink.custom.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * This controller is used to fetch available distance and
 * used(Round Trip) distance
 * 
 * @author santosh
 * @company Mathologic Technologies Pvt. Ltd.
 * @date Feb 5, 2022
 */
@Repository
public class UserVMRepositoryImpl implements UserVMRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("rawtypes")
    @Transactional(readOnly = false)
    @Override
    public List getAllUser(String sort, Long page, Long size) {
        List result = null;

        // String query0 = "SELECT COUNT(tt.id) , uu.username " + " FROM user AS uu
        // ORDER BY uu.id " +
        // "LIMIT "+size;
        String query0 = "SELECT * " + " FROM user AS uu ORDER BY uu.id " +
                "LIMIT :size "
                + "OFFSET :page ";
        try {
            Query query0f = entityManager.createNativeQuery(query0);
            // query0f.setParameter("sort", sort);
            query0f.setParameter("page", page);
            query0f.setParameter("size", size);
            // Long totalElements = ((java.math.BigInteger)
            // query0f.getSingleResult()).longValue();
            result = query0f.getResultList();

        } catch (Exception ex) {
            throw ex;
        }

        return result;
    }

    @Override
    public List getTotalUsedDistance(Long userPlan, String station) {
        return null;
    }

    @Override
    public List getTotalAvailableDistanceFrom(Long userPlan, String station) {
        return null;
    }

    @Override
    public List getTotalAvailableDistanceTo(Long userPlan, String station) {
        return null;
    }

    @Override
    public List getTotalAvailableDistanceBoth(Long userPlan, String station) {
        return null;
    }

}
