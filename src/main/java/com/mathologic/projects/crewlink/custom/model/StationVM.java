package com.mathologic.projects.crewlink.custom.model;

import java.io.Serializable;

public class StationVM implements Serializable {

    private static final long serialVersionUID = 1L;
    private String code;
    private String name;
    private Long head_station_sign_off_time;
    private String head_station_sign_on_time;
    private Long out_station_sign_on_time;
    private Long out_station_sign_off_time;
    private Long no_of_beds;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getHead_station_sign_off_time() {
        return head_station_sign_off_time;
    }

    public void setHead_station_sign_off_time(Long head_station_sign_off_time) {
        this.head_station_sign_off_time = head_station_sign_off_time;
    }

    public String getHead_station_sign_on_time() {
        return head_station_sign_on_time;
    }

    public void setHead_station_sign_on_time(String head_station_sign_on_time) {
        this.head_station_sign_on_time = head_station_sign_on_time;
    }

    public Long getOut_station_sign_on_time() {
        return out_station_sign_on_time;
    }

    public void setOut_station_sign_on_time(Long out_station_sign_on_time) {
        this.out_station_sign_on_time = out_station_sign_on_time;
    }

    public Long getOut_station_sign_off_time() {
        return out_station_sign_off_time;
    }

    public void setOut_station_sign_off_time(Long out_station_sign_off_time) {
        this.out_station_sign_off_time = out_station_sign_off_time;
    }

    public Long getNo_of_beds() {
        return no_of_beds;
    }

    public void setNo_of_beds(Long no_of_beds) {
        this.no_of_beds = no_of_beds;
    }

    public void put(Object column, Object value) {
        if (((String) column).equals("name")) {
            setName((String) value);
        } else if (((String) column).equals("code")) {
            setCode((String) value);
        }
    }

}
