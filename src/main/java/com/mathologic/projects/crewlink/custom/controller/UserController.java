package com.mathologic.projects.crewlink.custom.controller;


import com.mathologic.projects.crewlink.custom.repository.UserVMRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/api/custom/user")
public class UserController {

    @Autowired
    UserVMRepository userVMRepository;

    @RequestMapping(value = "/listAllUsers", method = RequestMethod.GET)
    public @ResponseBody
    List listAllUsers(
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false, defaultValue = "0") Long page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Long size) {
        List result = null;
        result = userVMRepository.getAllUser(sort,page,size);
        return result;
    }
}
