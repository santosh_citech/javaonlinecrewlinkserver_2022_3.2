package com.mathologic.projects.crewlink.custom.model;

import java.io.Serializable;

/**
 * @author : ADMIN 2/19/2022
 * @created : 19 / 02 / 2022 - 7:37 PM
 **/
public class RoleVM implements Serializable {

    private Long id;
    private String roleName;
    private String description;
    private Boolean can_create_user;
    private Boolean can_delete_user;
    private Boolean can_update_user;
    private Boolean can_view_dashboard;
    private Boolean can_view_user;

    public RoleVM(Long id, String roleName, String description, Boolean can_create_user, Boolean can_delete_user,
            Boolean can_update_user, Boolean can_view_dashboard, Boolean can_view_user) {
        super();
        this.id = id;
        this.roleName = roleName;
        this.description = description;
        this.can_create_user = can_create_user;
        this.can_delete_user = can_delete_user;
        this.can_update_user = can_update_user;
        this.can_view_dashboard = can_view_dashboard;
        this.can_view_user = can_view_user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCan_create_user() {
        return can_create_user;
    }

    public void setCan_create_user(Boolean can_create_user) {
        this.can_create_user = can_create_user;
    }

    public Boolean getCan_delete_user() {
        return can_delete_user;
    }

    public void setCan_delete_user(Boolean can_delete_user) {
        this.can_delete_user = can_delete_user;
    }

    public Boolean getCan_update_user() {
        return can_update_user;
    }

    public void setCan_update_user(Boolean can_update_user) {
        this.can_update_user = can_update_user;
    }

    public Boolean getCan_view_dashboard() {
        return can_view_dashboard;
    }

    public void setCan_view_dashboard(Boolean can_view_dashboard) {
        this.can_view_dashboard = can_view_dashboard;
    }

    public Boolean getCan_view_user() {
        return can_view_user;
    }

    public void setCan_view_user(Boolean can_view_user) {
        this.can_view_user = can_view_user;
    }
}
