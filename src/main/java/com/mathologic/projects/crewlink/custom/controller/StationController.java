package com.mathologic.projects.crewlink.custom.controller;


import com.mathologic.projects.crewlink.custom.model.StationVM;
import com.mathologic.projects.crewlink.custom.model.StoredProcedureProcessResult;
import com.mathologic.projects.crewlink.custom.repository.StationVMRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/api/custom/station")
public class StationController {

    @Autowired
    StationVMRepository stationVMRepository;

    @RequestMapping(value = "/listAllStations", method = RequestMethod.GET)
    public @ResponseBody
    List<?> listAllStations(
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false, defaultValue = "0") Long page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Long size) {
        List<?> result = null;
        result = stationVMRepository.getAllStations(sort,page,size);
        return result;
    }


    @RequestMapping(value = "/listAllStationsByProcedure", method = RequestMethod.GET)
    public @ResponseBody
    StoredProcedureProcessResult listAllStationsByProcedure(
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false, defaultValue = "0") Long page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Long size) {
           StoredProcedureProcessResult result = null;
        result = stationVMRepository.getStationsByProcedure(sort,page,size);
        return result;
    }
}
