package com.mathologic.projects.crewlink.custom.repository;

import com.jayway.jsonpath.internal.function.Parameter;
import com.mathologic.projects.crewlink.custom.model.StationVM;
import com.mathologic.projects.crewlink.custom.model.StoredProcedureProcessResult;
import com.mathologic.projects.crewlink.models.Station;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This controller is used to fetch station records using
 * stored procdue and native query
 * 
 * @author santosh
 * @company Mathologic Technologies Pvt. Ltd.
 * @date Feb 5, 2022
 */
@Repository
public class StationVMRepositoryImpl implements StationVMRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("rawtypes")
    @Transactional(readOnly = false)
    @Override
    public List<?> getAllStations(String sort, Long page, Long size) {
        List<?> list = null;
        String from = " FROM station AS ss ORDER BY :sort LIMIT :size OFFSET :page";

        String query1 = "SELECT ss.id, " +
                "ss.code, " +
                "ss.name, " +
                "ss.head_station_sign_off_time, " +
                "ss.head_station_sign_on_time, " +
                "ss.out_station_sign_off_time, " +
                "ss.out_station_sign_on_time, " +
                "ss.no_of_beds, " +
                "ss.mark_delete "
                + from;

        try {

            Query query0f = entityManager.createNativeQuery(query1);
            query0f.setParameter("page", page);
            query0f.setParameter("size", size);
            query0f.setParameter("sort", sort);

            list = query0f.getResultList();



        } catch (Exception ex) {
            throw ex;
        }

        return list;
    }

    @Override
    public StoredProcedureProcessResult getStationsByProcedure(String sort, Long page, Long size) {
        StoredProcedureProcessResult result = null;
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("stationRecordProcedure");

        storedProcedure.registerStoredProcedureParameter("sort", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("page", Long.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("size", Long.class, ParameterMode.IN);

        storedProcedure.registerStoredProcedureParameter("result", Boolean.class, ParameterMode.OUT);
        storedProcedure.registerStoredProcedureParameter("errorMessage", String.class, ParameterMode.OUT);

        storedProcedure.setParameter("sort", sort);
        storedProcedure.setParameter("page", page);
        storedProcedure.setParameter("size", size);

        storedProcedure.execute();

        Boolean resultB = (Boolean) storedProcedure.getOutputParameterValue("result");
        String errorMessage = storedProcedure.getOutputParameterValue("errorMessage").toString();

        if (resultB) {

            String outputValue = "Executed procedure :".toString();
            result = new StoredProcedureProcessResult(resultB, errorMessage, outputValue.toString());
        } else {
            result = new StoredProcedureProcessResult(resultB, errorMessage);
        }
        return result;
    }

}
