package com.mathologic.projects.crewlink.utility.library;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;
import static java.time.temporal.ChronoUnit.MINUTES;


public class TimeCalculator implements Serializable {

    public DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy H:m:s");

    public Long toMins(String s) {
        String[] hourMin = s.split(":");
        int hour = Integer.parseInt(hourMin[0]);
        int mins = Integer.parseInt(hourMin[1]);
        int hoursInMins = hour * 60;
        return Long.valueOf(hoursInMins) + Long.valueOf(mins);
    }

    public int hrsToMinutes(String time) {
        String[] parts = time.split(":");
        return Integer.parseInt( parts[0]) * 60 + Integer.parseInt(parts[1]);
    }
    private String convertTime(long time) {
        String finalTime = "";
        long hour = (time%(24*60)) / 60;
        long minutes = (time%(24*60)) % 60;
        long seconds = time / (24*3600);
        finalTime = String.format("%02d:%02d:%02d",
                TimeUnit.HOURS.toHours(hour) ,
                TimeUnit.MINUTES.toMinutes(minutes),
                TimeUnit.SECONDS.toSeconds(seconds));
        return finalTime;
    }



    private int conversionOfDateTimeToNumber(DateTimeModel dateTimeObj, String target){
        int totalMinutes = 60;
        int totalDayInMin = 1440 ; /*Since one day is equal to (24*60)= 1,440 minutes */
        int[] timesPartValue = new int[3];
        if (target == null) {
            target = "";
        }
        target = target.toLowerCase();
        String[] timePartStr = dateTimeObj.time.split(":");
        timesPartValue[0] = Integer.parseInt(timePartStr[0]);
        timesPartValue[1] = Integer.parseInt(timePartStr[1]);
        timesPartValue[2] = Integer.parseInt(timePartStr[2]);

        if((timesPartValue[0]>23 || timesPartValue[0]<0)&&(timesPartValue[1]>59 || timesPartValue[1]<0)){
            throw new RuntimeException("Not valid dateTimeObj.time passed to convertDateTimeObj()");
        }
        if(timesPartValue[2]<0){
            throw new RuntimeException("Not valid dateTimeObj.time passed to convertDateTimeObj()");
        }

        int mins = (dateTimeObj.day * totalDayInMin)
                + (timesPartValue[0] * totalMinutes)
                + (timesPartValue[1])
                + (timesPartValue[2]);

        int result;
        switch (target) {
            case "mins": case "min": case "minutes": case "minute":
                result = mins;
                break;
            case "hrs": case "hr": case "hours": case "hour":
                result = (mins / 60);
                break;
            case "days": case "day":
                result = (mins / 1440);
                break;
            default:
                result = mins;
                break;
        }
        return result;

    }

    public DateTimeModel conversionOfNumberToDateTimeModel(int number, String expression) {

        if (expression == null) {
            expression = "";
        }
        expression = expression.toLowerCase();

        int day = -1;
        int hrs = -1;
        int mins = -1;
        switch (expression) {
            case "mins": case "min": case "minutes": case "minute":
                day = (int) Math.floor(number / 1440);
                number = number % 1440;
                hrs = (int) Math.floor(number / 60);
                number = number % 60;
                mins = (int) Math.floor(number);
                if (mins > 60) {
                    throw new Error("Number not in correct type given");
                }
                break;
            case "hrs": case "hr": case "hours": case "hour":
                day = (int) Math.floor(number / 1440);
                number = number % 1440;
                hrs = (int) Math.floor(number / 60);
                if (hrs > 24) {
                    throw new Error("Number not in correct type given");
                }
                break;
            case "days": case "day":
                day = (int) Math.floor(number / 1440);
                if (day > 24) {
                    throw new Error("Number not in correct type given");
                }
                break;
            default:
                day = (int) Math.floor(number / 1440);
                number = number % 1440;
                hrs = (int) Math.floor(number / 60);
                number = number % 60;
                mins = (int) Math.floor(number);
                if (mins > 60) {
                    throw new Error("Number not in correct type given");
                }
                break;
        }
        DateTimeModel result = new DateTimeModel();
        if (day != -1) {
            result.days = day;
            result.day = day % 7;
        }
        if (hrs != -1 && mins == -1) {
            result.time = (hrs < 10) ? "0" : "" + hrs + ":00";
        }
        else if (hrs != -1 && mins != -1) {
            result.time = "";
            result.time += ((hrs < 10) ? "0" : "") + hrs;
            result.time += ":" + ((mins < 10) ? "0" : "") + mins;
        }
        return result;
    }

    public Long findDiff(Integer fromDay, String fromTime, Integer toDay, String toTime,ChronoUnit unit){
        Long result = findDiffWeekly(fromDay, fromTime, toDay, toTime, unit);
        if(result<0){
            switch(unit){
                case MINUTES:
                    result = (7*24*60) + result;
                    break;

                case HOURS:
                    result = (7*24)+result;
                    break;

                case SECONDS:
                    result = (7*24*60*60) + result;
                    break;
            }
        }
        return result;
    }

    public Long findDiffWeekly(Integer fromDay, String fromTime, Integer toDay, String toTime, ChronoUnit unit){
        LocalDateTime fromDateTime = LocalDateTime.parse("1/01/2022 "+ fromTime, formatter);

        Integer diff = toDay - fromDay;
        if (diff < 0) {
            diff = 7 + diff;
        }

        LocalDateTime toDateTime = LocalDateTime.parse((1 + diff) + "/01/2022 "+ toTime,formatter);
        return unit.between(fromDateTime, toDateTime);
    }
/*
    public static void main(String args[]){
        DateTimeModel ddmm = new DateTimeModel();
        ddmm.day = 1;
        ddmm.time ="07:10:00";
        ddmm.days = 4;
        ddmm.number= 2;
        ddmm.numberType="mins";
        new TimeCalculator().conversionOfDateTimeToNumber(ddmm,"days");
        System.out.println(new TimeCalculator().conversionOfNumberToDateTimeModel(2440,"min"));
        System.out.println(new TimeCalculator().findDiff(1,"07:22:00",1,"17:20:00",MINUTES));

    }
    */


}
