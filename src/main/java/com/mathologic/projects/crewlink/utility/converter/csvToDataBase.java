package com.mathologic.projects.crewlink.utility.converter;

import org.springframework.stereotype.Service;

@Service
public interface csvToDataBase {
    void processRecords(String [] columns);
}
