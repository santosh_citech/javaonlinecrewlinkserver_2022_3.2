package com.mathologic.projects.crewlink.utility.converter;

import java.sql.Time;
import java.time.LocalTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Converter to persist LocalDate and LocalDateTime with
 * JPA 2.1 and Hibernate older than 5.0 version
 **/

@Converter(autoApply = true)
public class LocalTimeAttributeConverter implements AttributeConverter<LocalTime, Time>{

    @Override
    public Time convertToDatabaseColumn(LocalTime localTime) {
        if(localTime!=null){
            return Time.valueOf(localTime);
        }
        return null;
    }

    @Override
    public LocalTime convertToEntityAttribute(Time time) {
        if(time!=null){
           return time.toLocalTime();
        }
        return null;
    }

}