package com.mathologic.projects.crewlink.utility.library;

/**
 * @author : ADMIN 2/16/2022
 * @created : 16 / 02 / 2022 - 2:55 AM
 **/
class DateTimeOBJ {
    public int day;
    public String time;
    public int days;
    public int number;
    public String numberType;

    public DateTimeOBJ() {
        super();
    }

    @Override
    public String toString() {
        return "day: " + this.day +
                " time: " + this.time +
                " days: " + this.days +
                " number: " + this.number +
                " Type: " + this.numberType;
    }
}

public class TimeUtility {

    public static void main(String[] args){
        new TimeUtility().conversionOfNumberToDateTimeModel(1558,"minutes");

    }

    public DateTimeOBJ conversionOfNumberToDateTimeModel(int number, String expression) {

        if (expression == null) {
            expression = "";
        }
        expression = expression.toLowerCase();

        int day = -1;
        int hrs = -1;
        int mins = -1;
        switch (expression) {
            case "mins": case "min": case "minutes": case "minute":
                day = (int) Math.floor(number / 1440);
                number = number % 1440;
                hrs = (int) Math.floor(number / 60);
                number = number % 60;
                mins = (int) Math.floor(number);
                if (mins > 60) {
                    throw new Error("Number not in correct type given");
                }
                break;
            case "hrs": case "hr": case "hours": case "hour":
                day = (int) Math.floor(number / 1440);
                number = number % 1440;
                hrs = (int) Math.floor(number / 60);
                if (hrs > 24) {
                    throw new Error("Number not in correct type given");
                }
                break;
            case "days": case "day":
                day = (int) Math.floor(number / 1440);
                if (day > 24) {
                    throw new Error("Number not in correct type given");
                }
                break;
            default:
                day = (int) Math.floor(number / 1440);
                number = number % 1440;
                hrs = (int) Math.floor(number / 60);
                number = number % 60;
                mins = (int) Math.floor(number);
                if (mins > 60) {
                    throw new Error("Number not in correct type given");
                }
                break;
        }
        DateTimeOBJ result = new DateTimeOBJ();
        if (day != -1) {
            result.days = day;
            result.day = day % 7;
        }
        if (hrs != -1 && mins == -1) {
            result.time = (hrs < 10) ? "0" : "" + hrs + ":00";
        }
        else if (hrs != -1 && mins != -1) {
            result.time = "";
            result.time += ((hrs < 10) ? "0" : "") + hrs;
            result.time += ":" + ((mins < 10) ? "0" : "") + mins;
        }
        return result;
    }

    public Integer convertDateTimeObjToNumber(DateTimeOBJ dateTimeObj, String target) {
        if (dateTimeObj == null) {
            throw new Error("Not valid dateTimeObject passed to convertDateTimeObj()");
        }
        if (dateTimeObj.day == 0 || dateTimeObj.time == null) {
            throw new Error("Not valid dateTimeObject passed to convertDateTimeObj()");
        }
        if (target == null) {
            target = "";
        }
        target = target.toLowerCase();
        int[] timesPartValue = new int[3];
        String[] timeParts = dateTimeObj.time.split(":");
        timesPartValue[0] = Integer.parseInt(timeParts[0]);
        timesPartValue[1] = Integer.parseInt(timeParts[1]);
        if ((timesPartValue[0] > 23 || timesPartValue[0] < 0) && (timesPartValue[1] > 59 || timesPartValue[1] < 0)) {
            throw new RuntimeException("Not valid dateTimeObj.time passed to convertDateTimeObj()");
        }

        var mins = (dateTimeObj.day * 1440) + (timesPartValue[0] * 60) + (timesPartValue[1]);

        int result;
        switch (target) {
            case "mins":
            case "min":
            case "minutes":
            case "minute":
                result = mins;
                break;
            case "hrs":
            case "hr":
            case "hours":
            case "hour":
                result = (mins / 60);
                break;
            case "days":
            case "day":
                result = (mins / 1440);
                break;
            default:
                result = mins;
                break;
        }
        return result;
    }
}
