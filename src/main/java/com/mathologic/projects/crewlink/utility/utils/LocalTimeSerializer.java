package com.mathologic.projects.crewlink.utility.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.LocalTime;

public class LocalTimeSerializer extends JsonSerializer<LocalTime> {
    @Override
    public void serialize(LocalTime args0, JsonGenerator args1, SerializerProvider args2) throws IOException {
        args1.writeString(args0.toString());
    }
}
