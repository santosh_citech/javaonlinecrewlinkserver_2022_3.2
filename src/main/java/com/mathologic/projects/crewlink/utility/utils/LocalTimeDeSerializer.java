package com.mathologic.projects.crewlink.utility.utils;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class LocalTimeDeSerializer extends JsonDeserializer<LocalTime> {
    @Override
    public LocalTime deserialize(JsonParser args0, DeserializationContext args1) throws IOException, JacksonException {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_TIME;
        return LocalTime.parse(args0.getText(),formatter);
    }
}
