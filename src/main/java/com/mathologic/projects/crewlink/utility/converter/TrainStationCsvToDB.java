package com.mathologic.projects.crewlink.utility.converter;

import com.mathologic.projects.crewlink.services.dao.TrainStationVMDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("TrainTimeTable")
public class TrainStationCsvToDB implements csvToDataBase {

    @Autowired
    TrainStationVMDao trainStationVMDao;
    @Override
    public void processRecords(String[] columns) {

        if(!(columns.length==0)){
            Integer trainNo = Integer.parseInt(columns[0]);
            Integer stopNo = Integer.parseInt(columns[1]);
            String code =columns[2];
            Long DayOfJourney = Long.valueOf(columns[3]);
            String arrivalTime = columns[4];
            String departureTime  = columns[5];
            Long distance = Long.valueOf(columns[6]);
            String locoType = columns[7];
            trainStationVMDao.saveTrainStation(trainNo,stopNo,code,DayOfJourney,arrivalTime,departureTime,distance,locoType);

        }

    }
}