package com.mathologic.projects.crewlink.utility.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Time;
import java.time.LocalTime;


//@Converter(autoApply = true)
@SuppressWarnings("unsafe")
@Converter
public class LocalTimeConverter implements AttributeConverter<LocalTime, Time> {
    @Override
    public Time convertToDatabaseColumn(LocalTime localTime) {
        if(localTime == null){
            return null;
        }
        Time sqlTime = Time.valueOf(localTime);
        return sqlTime;
    }

    @Override
    public LocalTime convertToEntityAttribute(Time time) {
        if(time == null){
            return null;
        }

        LocalTime localTime = time.toLocalTime();
        return localTime;
    }
}
