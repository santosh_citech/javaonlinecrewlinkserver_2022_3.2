package com.mathologic.projects.crewlink.enums;

/**
 * @author : ADMIN 2/18/2022
 * @created : 18 / 02 / 2022 - 8:00 PM
 **/
public enum RoleEnum {
    ADMIN,USER,SUPER,SUPERUSER,EMPLOYEE,MANAGER,PROGRAMMER,CHIEF,MODERATOR
}
