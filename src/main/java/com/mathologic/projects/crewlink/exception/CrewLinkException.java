package com.mathologic.projects.crewlink.exception;

public class CrewLinkException extends Exception {
    private static final String MSG_ERR_NUM1 = "Error number 1";
    private static final String MSG_ERR_NUM2 = "Error number 2";

    private static final long serialVersionUID = 1L;

    public CrewLinkException() {
        super();
    }

    public CrewLinkException(String message) {
        super(MSG_ERR_NUM1 + message);
    }

    public CrewLinkException(String message, Throwable cause) {
        super(MSG_ERR_NUM2 + message, cause);
    }

    public CrewLinkException(Throwable cause) {
        super(cause);
    }
}
