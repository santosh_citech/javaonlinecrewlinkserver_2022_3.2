package com.mathologic.projects.crewlink.services.daoImpl;

import com.mathologic.projects.crewlink.custom.model.TrainVM;
import com.mathologic.projects.crewlink.models.Trains;
import com.mathologic.projects.crewlink.models.selection.PaginationDetails;
import com.mathologic.projects.crewlink.models.selection.SelectionModel;
import com.mathologic.projects.crewlink.services.dao.TrainVMRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TrainVMImplRepository implements TrainVMRepository {

    @PersistenceContext
    private final EntityManager entityManager;
    private static final Logger log = LoggerFactory.getLogger(TrainVMImplRepository.class);

    public TrainVMImplRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public Boolean saveTrainStoredProcedure(Integer trainNo, String TrainName, String fromStation, String toStation,
            String startDay, String trainType) {

        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("TrainUpload");
        storedProcedure.registerStoredProcedureParameter("TrainNo", Integer.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("TrainName", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("fromstation", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("tostation", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("startday", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("traintype", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("createdTrainId", Long.class, ParameterMode.OUT);
        storedProcedure.registerStoredProcedureParameter("result", Boolean.class, ParameterMode.OUT);
        storedProcedure.registerStoredProcedureParameter("errormessage", String.class, ParameterMode.OUT);
        storedProcedure.setParameter("TrainNo", trainNo);
        storedProcedure.setParameter("TrainName", TrainName);
        storedProcedure.setParameter("fromstation", fromStation);
        storedProcedure.setParameter("tostation", toStation);
        storedProcedure.setParameter("startday", startDay);
        storedProcedure.setParameter("traintype", trainType);

        storedProcedure.execute();
        Boolean resultB = (Boolean) storedProcedure.getOutputParameterValue("result");
        String errorMessage = storedProcedure.getOutputParameterValue("errormessage").toString();

        log.info("Train upload using stored procedure successfully!!" + errorMessage);
        if (resultB) {
            Long outputValue = (Long) storedProcedure.getOutputParameterValue("createdTrainId");
            log.info("Train upload using stored procedure successfully!!" + outputValue);

        } else {
            log.info("Unable to upload train details using stored procedure!!");

        }

        return true;
    }

    private BigInteger findFromStation(String fromStation) {
        try {
            if (!fromStation.isEmpty()) {
                String q1 = "SELECT ss.id FROM station AS ss WHERE ss.code LIKE :fromStation";
                Query query = entityManager.createNativeQuery(q1);
                query.setParameter("fromStation", "%" + fromStation + "%");
                return (BigInteger) query.getResultList().get(0);
            }
        } catch (Exception exe) {
            System.out.println("Error in findFromStation(): " + exe);
        }
        return null;
    }

    private BigInteger findToStation(String code) {
        try {
            if (!code.isEmpty()) {
                String q1 = "SELECT ss.id FROM station AS ss WHERE ss.code LIKE :code";
                Query query = entityManager.createNativeQuery(q1);
                query.setParameter("code", "%" + code + "%");
                return (BigInteger) query.getResultList().get(0);

            }
        } catch (Exception exe) {
            System.out.println("Error in findToStation(): " + exe);
        }

        return null;
    }

    private BigInteger findTrainType(String Name) {
        try {
            if (!Name.isEmpty()) {
                String q1 = "SELECT tt.id FROM train_type AS tt WHERE tt.name LIKE :Name ";
                Query query = entityManager.createNativeQuery(q1);
                query.setParameter("Name", "%" + Name + "%");
                return (BigInteger) query.getResultList().get(0);
            }
        } catch (Exception exe) {
            System.out.println("Error in findTrainType(): " + exe);
        }
        return null;
    }

    private void insertInDB(
            Integer trainNo,
            String trainName,
            String from,
            String to,
            Integer startDay,
            String trainType,
            Long sourceId,
            Long destinationId,
            Long trainTypeId) {

        try {
            String q1 = "INSERT INTO train" +
                    "(train_no,train_name,from_station,to_station,start_day,train_type,source_id,destination_id,train_type_id)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            Query query = entityManager.createNativeQuery(q1);
            query.setParameter(1, trainNo);
            query.setParameter(2, trainName);
            query.setParameter(3, from);
            query.setParameter(4, to);
            query.setParameter(5, startDay);
            query.setParameter(6, trainType);

            query.setParameter(7, sourceId);
            query.setParameter(8, destinationId);
            query.setParameter(9, trainTypeId);
            query.executeUpdate();

        } catch (Exception exe) {
            System.out.println("Error in insertInDB() " + exe.getMessage());
        }

    }

    @Override
    @Transactional
    public Boolean saveTrainUsingNative(List<Trains> trains) {
        long fromId;
        long toId;
        long trainTypeId;
        try {

            for (Trains train : trains) {

                fromId = this.findFromStation(train.getFromStation()).longValue();
                toId = this.findToStation(train.getToStation()).longValue();
                trainTypeId = this.findTrainType(train.getTrainType()).longValue();
                this.insertInDB(
                        train.getTrainNo(),
                        train.getTrainName(),
                        train.getFromStation(),
                        train.getToStation(),
                        train.getStartDay().ordinal(),
                        train.getTrainType(),
                        fromId,
                        toId,
                        trainTypeId);

            }
        } catch (Exception ex) {
            System.out.println("Error in saveTrainUsingNative() " + ex.getMessage());
            log.error("Error in saveTrainUsingNative() " + ex.getMessage());
        }
        return true;
    }

    @Transactional
    @Override
    public List<Map<Object, Object>> listTrains(String sort, Long limit, Long page, Long size) {

        String FROM = "	FROM train as tt"
                + "	LEFT JOIN station AS fs ON (fs.id = tt.source_id)"
                + "	LEFT JOIN station AS ts ON (ts.id = tt.destination_id)"
                + "	LEFT JOIN train_type AS tty ON (tty.id = tt.train_type_id)";
        String query1 = "SELECT COUNT(tt.id)"
                + FROM;
        try {
            Query query1f = entityManager.createNativeQuery(query1);
            Long totalElements = ((java.math.BigInteger) query1f.getSingleResult()).longValue();
            Long startIndex = (page * size);
            Long totalPages = totalElements / size;
            Long currentPage = page;

            if (totalElements > 0) {
                if (sort != null && sort.isEmpty()) {
                    sort = null;
                }

                String FROM0 = " FROM train as tt"
                        + "	LEFT JOIN station AS fs ON (tt.source_id=fs.id)"
                        + "	LEFT JOIN station AS ts ON (tt.destination_id=ts.id)"
                        + "	LEFT JOIN train_type AS tty ON (tty.id = tt.train_type_id)";
                String query2 = "SELECT tt.start_day as startDay," +
                        " tt.train_no as trainNo," +
                        " tt.train_name as trainName," +
                        " fs.code as fromStation," +
                        " ts.code as toStation," +
                        " fs.name as fromName," +
                        " ts.name as toName," +
                        " tty.name as TrainType"

                        + FROM0 + " LIMIT :start, :offset";
                Query query2f = entityManager.createNativeQuery(query2);
                query2f.setParameter("start", page * size);
                query2f.setParameter("offset", size);

                List<Object[]> resultList = (List<Object[]>) query2f.getResultList();
                List<Map<Object, Object>> ret = new ArrayList<Map<Object, Object>>();
                for (Object[] result : resultList) {
                    Map<Object, Object> row = new HashMap<>();
                    row.put("startDay", result[0]);
                    row.put("trainNo", result[1]);
                    row.put("trainName", result[2]);
                    row.put("fromStation", result[3]);
                    row.put("toStation", result[4]);
                    row.put("fromName", result[5]);
                    row.put("toName", result[6]);
                    row.put("trainType", result[7]);
                    ret.add(row);
                }
                return ret;

            }

        } catch (Exception e) {
            log.error("SQL Error" + e.getMessage());
        }
        return null;
    }

    @Override
    public SelectionModel listALLTrains(String sort, Long page, Long size) {
        Long totalElements = 0L;
        if (sort != null && sort.isEmpty()) {
            sort = null;
        }

        String to01 = "SELECT COUNT(distinct tt.id) ";

        String From01 = "FROM train AS tt LEFT JOIN station AS ss ON(tt.source_id=ss.id) " +
                "LEFT JOIN station AS ds ON(tt.destination_id = ds.id) " +
                "LEFT JOIN train_type AS ttp ON(tt.train_type_id = ttp.id)";

        String query0 = to01 + From01;
        try {
            Query query1f = entityManager.createNativeQuery(query0);

            totalElements = ((BigInteger) query1f.getSingleResult()).longValue();



        } catch (Exception e) {
            log.error("SQL ERROR IN Query01 :" + e);
        }

        String to = "SELECT " +
                "tt.train_no as trainNo," +
                "tt.train_name as trainName, " +
                "ss.code AS fromStation," +
                "ss.name AS fromStationName," +
                "tt.start_day as startDay ," +
                "ds.code AS destination, " +
                "ds.name AS destinationName, " +
                "ttp.name as trainType ";

        String From = "FROM train AS tt " +
                "LEFT JOIN station AS ss ON(tt.source_id=ss.id) " +
                "LEFT JOIN station AS ds ON(tt.destination_id = ds.id) " +
                "LEFT JOIN train_type AS ttp ON(tt.train_type_id = ttp.id) " +
                "ORDER BY :sort " +
                "LIMIT :size " +
                "OFFSET :page ";

        String query = to + From;
        try {
            Query query1 = entityManager.createNativeQuery(query);
            query1.setParameter("sort", sort);
            query1.setParameter("page", page);
            query1.setParameter("size", size);

            List<?> list = query1.getResultList();

            Long startIndex = (page * size);
            Long totalPages = (totalElements / size);

            String baseItemRestUri = "/listAllTrains";
            return new SelectionModel(TrainVM.class, list,
                    new PaginationDetails(totalElements,
                            startIndex,
                            page,
                            totalPages,
                            baseItemRestUri,
                            totalElements,
                            size,
                            page,
                            null, null, true, true),
                    null);
        } catch (Exception e) {
            log.error("SQL ERROR :" + e);
        }
        return null;
    }

}
