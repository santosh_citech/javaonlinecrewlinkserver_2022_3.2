package com.mathologic.projects.crewlink.services.dao;

import com.mathologic.projects.crewlink.models.Trains;
import com.mathologic.projects.crewlink.models.selection.SelectionModel;

import java.util.List;
import java.util.Map;

public interface TrainVMRepository {

    Boolean saveTrainStoredProcedure(Integer trainNo,  String name,
                            String fromStation, String toStation,String startDay,
                            String trainType);

    Boolean saveTrainUsingNative(List<Trains> trains);
    List<Map<Object, Object>> listTrains(String sort, Long limit, Long page, Long size);

    SelectionModel listALLTrains(String sort, Long page, Long size);
}
