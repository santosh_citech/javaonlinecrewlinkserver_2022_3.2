package com.mathologic.projects.crewlink.services.dao;

import com.mathologic.projects.crewlink.models.Premission;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author : ADMIN 2/18/2022
 * @created : 18 / 02 / 2022 - 1:52 AM
 **/
public interface PremissionService {
    void save(Premission premission);
    List<?> getAllPremission();
    List<Premission> findByAllParams(String sort, Long page, Long size);
}
