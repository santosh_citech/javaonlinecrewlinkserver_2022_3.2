package com.mathologic.projects.crewlink.services.dao;

import java.util.List;
import java.util.Map;

public interface TrainStationVMDao {
    /**
     * @param trainNo
     * @param stopNo
     * @param code
     * @param DayOfJourney
     * @param arrivalTime
     * @param departureTime
     * @param distance
     * @param locoType
     */
    void saveTrainStation(Integer trainNo, Integer stopNo, String code, Long DayOfJourney, String arrivalTime, String departureTime, Long distance, String locoType);

    /**
     * @param sort
     * @param limit
     * @param page
     * @param size
     * @return
     */
    List<Map<Object, Object>> list(String sort, Long limit, Long page, Long size);

}
