package com.mathologic.projects.crewlink.services;

import com.mathologic.projects.crewlink.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author : ADMIN 2/17/2022
 * @created : 17 / 02 / 2022 - 6:39 AM
 **/
@Service
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private final UserService userService;
    public MyUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        BCryptPasswordEncoder encoder = passwordEncoder();
        User user = userService.findUserByUserName(username);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", user.getUsername()));
        }else {
            return new org.springframework.security.core.userdetails.User(user.getUsername(),
                    encoder.encode(user.getPassword()), user.getActive(), true, true, true, getAuthorities(user));
        }

    }
    private Collection<? extends GrantedAuthority> getAuthorities(User user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        if(user.getRole().getRolename().name().equals("admin")||user.getRole().getRolename().name().equals("ADMIN")) {
            authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }
        if(user.getRole().getRolename().name().equals("user")||user.getRole().getRolename().name().equals("USER")) {
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        }
        if(user.getRole().getRolename().name().equals("super")||user.getRole().getRolename().name().equals("SUPER")) {
            authorities.add(new SimpleGrantedAuthority("ROLE_SUPER"));
        }


        System.out.print("authorities :"+authorities);
        return authorities;
    }
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
