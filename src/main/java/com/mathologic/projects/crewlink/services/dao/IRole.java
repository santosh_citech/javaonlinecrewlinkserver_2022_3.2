package com.mathologic.projects.crewlink.services.dao;

import com.mathologic.projects.crewlink.exception.CrewLinkException;
import com.mathologic.projects.crewlink.models.Role;
import com.mathologic.projects.crewlink.models.selection.SelectionModel;

import java.util.List;

public interface IRole {

    void saveInDb(Object object)throws CrewLinkException;


    SelectionModel getAllRoles(String sort, Long size, Long page)throws CrewLinkException;
}
