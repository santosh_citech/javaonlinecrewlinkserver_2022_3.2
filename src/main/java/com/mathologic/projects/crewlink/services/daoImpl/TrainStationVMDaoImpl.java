package com.mathologic.projects.crewlink.services.daoImpl;

import com.mathologic.projects.crewlink.services.dao.TrainStationVMDao;
import com.mathologic.projects.crewlink.utility.library.TimeCalculator;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TrainStationVMDaoImpl implements TrainStationVMDao {

    @PersistenceContext
    private final EntityManager entityManager;
    private static final Logger logger = LoggerFactory.getLogger(TrainStationVMDaoImpl.class);
    private final TimeCalculator timeCalculator;

    TrainStationVMDaoImpl(EntityManager entityManager){
        this.entityManager = entityManager;
        timeCalculator = new TimeCalculator();

    }




    private  BigInteger findStation(String code) {
        try {
            if (!code.isEmpty()) {
                String q1 = "SELECT ss.id FROM station AS ss WHERE ss.code LIKE :code";
                Query query = entityManager.createNativeQuery(q1);
                query.setParameter("code", "%" + code + "%");
                return (BigInteger) query.getResultList().get(0);
            }
        } catch (Exception exe) {
            System.out.println("Error in findToStation(): " + exe);
        }

        return null;
    }

    private BigInteger findTrain(Integer trainNo) {
        try {

            String q1 = "SELECT t.id FROM train AS t WHERE t.train_no = :trainNo";
            Query query = entityManager.createNativeQuery(q1);
            query.setParameter("trainNo", trainNo);
            return (BigInteger) query.getResultList().get(0);

        } catch (Exception exe) {
            System.out.println("Error in findTrain(): " + exe);
        }

        return null;
    }

    public List<Object[]> findIdAndStartDayByTrainNo(Integer trainNo) {
        List<Object[]> list = null;
        try {
            String q0 = "SELECT t.id,t.start_day FROM train as t WHERE t.train_no =:trainNo";
            Query query = entityManager.createNativeQuery(q0);
            query.setParameter("trainNo", trainNo);
            list = query.getResultList();


        } catch (Exception exe) {
            System.out.println("Error in findIdAndStartDayByTrainNo() " + exe.getMessage());
            logger.error("Error in findIdAndStartDayByTrainNo():" + exe.getMessage());
        }
        return list;
    }

    public java.math.BigInteger getRecordsCount(Integer trainNo){
        java.math.BigInteger TrainCount = null;

       try {
           String q0 = "SELECT COUNT(t.id) FROM train as t WHERE t.train_no =:trainNo";
           Query query = entityManager.createNativeQuery(q0);
           query.setParameter("trainNo", trainNo);
           TrainCount = (java.math.BigInteger) query.getSingleResult();
       }catch(Exception ex){
           System.out.println("Error in getRecordsCount() " + ex.getMessage());
           logger.error("Error in getRecordsCount():" + ex.getMessage());

       }
        return TrainCount;
    }

    public void calculateJourneyDuration(){

    }

    public BigInteger getTrainStationId(BigInteger t_id,Integer stopNumber){
        java.math.BigInteger tsId = null;
        try{
        String s0 ="SELECT ts.id FROM " +
                    "train_station as ts " +
                    "WHERE ts.train_id = :t_id AND ts.stop_number = :stopNumber";
        Query query = entityManager.createNativeQuery(s0);
            query.setParameter("t_id", t_id);
        query.setParameter("stopNumber", stopNumber);

       return tsId = (java.math.BigInteger) query.getSingleResult();
    }catch(Exception ex){
        System.out.println("Error in getTrainStationId() " + ex.getMessage());
        logger.error("Error in getTrainStationId():" + ex.getMessage());

    }
        return null;
    }

    public boolean saveStation(BigInteger stationId, String stationCode){
        int result = 0;
        if(stationId==null) {

            StringBuilder q0 = new StringBuilder();
            q0.append("INSERT INTO station");
            q0.append("(code, " +
                    "head_station_sign_off_time, " +
                    "head_station_sign_on_time, " +
                    "name, " +
                    "number_of_beds, " +
                    "out_station_sign_off_time, " +
                    "out_station_sign_on_time)");
            q0.append(" VALUES(?, ?, ?,?, ?, ?, ?)");

            Query query = entityManager.createNativeQuery(q0.toString());
            query.setParameter(1, stationCode);
            query.setParameter(2, 30);
            query.setParameter(3, 30);
            query.setParameter(4, "");
            query.setParameter(5, 1);
            query.setParameter(6, 30);

            query.setParameter(7, 30);
            result =  query.executeUpdate();
            if (result==1)
                return true;
            else
                return false;

        }
      return false;
    }

    @Override
    @Transactional
    public void saveTrainStation(Integer trainNo, Integer stopNo, String code, Long DayOfJourney, String arrivalTime,
            String departureTime, Long distance, String locoType) {
        BigInteger  t_id = null;
        Integer startDay;
        try {
            BigInteger trainCount = getRecordsCount(trainNo);
            List<Object[]> list =  findIdAndStartDayByTrainNo(trainNo);
            for(Object[] q1 : list){
                t_id = ( java.math.BigInteger )q1[0];
                 startDay = (Integer) q1[1];
            }
            BigInteger tsid = getTrainStationId(t_id,stopNo);
            BigInteger station = findStation(code);
            if(saveStation(station,code)){
                System.out.println("Station saved Successfully!!");
            }else{
                System.out.println("Station not saved Successfully!!");
            }


            Long arrivalInMin = timeCalculator.toMins(arrivalTime);
            Long departureInMin = timeCalculator.toMins(departureTime);
            Long codeId = findStation(code).longValue();
            Long trainId = findTrain(trainNo).longValue();
            DateTimeFormatter parser = DateTimeFormatter.ofPattern("H:mm");
            LocalTime arriavllocalTime = LocalTime.parse(arrivalTime, parser);
            LocalTime departurelocalTime = LocalTime.parse(departureTime, parser);

            StringBuilder q0 = new StringBuilder();
            q0.append("INSERT INTO train_station");
            q0.append("(train_no," +
                    "stop_no," +
                    "station_code," +
                    "day_of_journey," +
                    "arrival_time," +
                    "departure_time," +
                    "distance," +
                    "loco_type," +
                    "arrival_in_minutes," +
                    "departure_in_minutes," +
                    "train_id," +
                    "station_id" +
                    ")");
            q0.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            Query query = entityManager.createNativeQuery(q0.toString());
            query.setParameter(1, trainNo);
            query.setParameter(2, stopNo);
            query.setParameter(3, code);
            query.setParameter(4, DayOfJourney);
            query.setParameter(5, arriavllocalTime);
            query.setParameter(6, departurelocalTime);

            query.setParameter(7, distance);
            query.setParameter(8, locoType);
            query.setParameter(9, arrivalInMin);
            query.setParameter(10, departureInMin);
            query.setParameter(11, trainId);
            query.setParameter(12, codeId);
//            query.executeUpdate();

        } catch (HibernateException exe) {
            System.out.println("Error in insertInDB() " + exe.getMessage());
            logger.error("Error in insertInDB() " + exe.getMessage());
        }

    }

    @Override
    @Transactional
    public List<Map<Object, Object>> list(String sort, Long limit, Long page, Long size) {

        String FROM = "	FROM train_station as ts"
                + "	LEFT JOIN station AS fs ON (ts.station_id = fs.id)"
                + "	LEFT JOIN train AS tt ON (tt.id = ts.train_id)";
        String query1 = "SELECT COUNT(ts.id)"
                + FROM;
        try {
            Query query1f = entityManager.createNativeQuery(query1);
            Long totalElements = ((java.math.BigInteger) query1f.getSingleResult()).longValue();
            Long startIndex = (page * size);
            Long totalPages = totalElements / size;
            Long currentPage = page;

            if (totalElements > 0) {
                if (sort != null && sort.isEmpty()) {
                    sort = null;
                }

                String FROM0 = " FROM train_station as ts"
                        + "	LEFT JOIN station AS fs ON (ts.station_id = fs.id)"
                        + "	LEFT JOIN train AS tt ON (tt.id = ts.train_id)"
                        + "	INNER JOIN train_type AS tty ON (tty.id = tt.train_type_id)";
                String query2 = "SELECT ts.station_code as code," +
                        " tt.train_no as trainNo," +
                        " ts.arrival_time as arrival," +
                        " ts.departure_time as departure,"+
                        " tt.train_name as trainName,"+
                        " tty.name as TrainType"

                        + FROM0 + " LIMIT :start, :offset";
                Query query2f = entityManager.createNativeQuery(query2);
                query2f.setParameter("start", page * size);
                query2f.setParameter("offset", size);

                List<Object[]> resultList = (List<Object[]>) query2f.getResultList();
                List<Map<Object, Object>> ret = new ArrayList<Map<Object, Object>>();
                for (Object[] result : resultList) {
                    Map<Object, Object> row = new HashMap<>();
                    row.put("code", result[0]);
                    row.put("trainNo", result[1]);
                    row.put("arrivalTime", result[2]);
                    row.put("departureTime", result[3]);
                    row.put("trainName", result[4]);
                    row.put("trainType", result[5]);
                    ret.add(row);
                }
                return ret;

            }

        } catch (Exception e) {
            logger.error("SQL Error" + e.getMessage());
        }

        return null;
    }
}