package com.mathologic.projects.crewlink.services.daoImpl;

import com.mathologic.projects.crewlink.custom.model.RoleVM;
import com.mathologic.projects.crewlink.exception.CrewLinkException;
import com.mathologic.projects.crewlink.models.Role;
import com.mathologic.projects.crewlink.models.selection.Pagination;
import com.mathologic.projects.crewlink.models.selection.SelectionModel;
import com.mathologic.projects.crewlink.services.dao.IRole;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@Service
@SuppressWarnings("SpellCheckingInspection")
public class IRoleImpl implements IRole {

    @PersistenceContext
    private final EntityManager entityManager;
    private static final Logger log = LoggerFactory.getLogger(IRoleImpl.class);

    public IRoleImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public boolean saveOrUpdateObject(Object object) throws HibernateException {
        boolean isSaved = Boolean.FALSE;
        String query = "INSERT INTO role (name)" + " VALUES ( :a)";
        try {
            if (object != null) {
                // entityManager.persist(object);
                Role r = (Role) object;
                entityManager.createNativeQuery(query)
                        .setParameter("a", r.getRolename())
                        .executeUpdate();
                return isSaved = Boolean.TRUE;
            }
        } catch (HibernateException ex) {
            log.error("Error :  Error in saveOrUpdate Object ", ex);
        }
        return isSaved;
    }

    @Transactional
    @Override
    public void saveInDb(Object object) throws CrewLinkException {
        saveOrUpdateObject(object);

    }

    @SuppressWarnings("SpellCheckingInspection")
    public Long getPagination() throws CrewLinkException {
        Long result = 0L;

        String sb = "SELECT " +
                " COUNT(role_premissions.roles_id)" +
                " FROM role" +
                " JOIN role_premissions ON (role.id = role_premissions.roles_id)" +
                " JOIN premission ON (premission.id = role_premissions.premissions_id)";

        try {
            Query query2f = entityManager.createNativeQuery(sb);
            Object obj = query2f.getSingleResult();
            if (obj != null) {
                result = Long.valueOf(obj.toString());
            }
        } catch (Exception ex) {
            throw new CrewLinkException("cast based " + ex, ex);
        }

        return result;

    }

    @Transactional
    @Override
    public SelectionModel getAllRoles(String sort, Long size, Long page) throws CrewLinkException {

        Long totalElements = getPagination();

        String sb = "SELECT" +
                " role.rolename,premission.can_create_users, " +
                " premission.can_delete_users,premission.can_update_users," +
                " premission.can_view_users,premission.can_view_dashboard" +
                " FROM role" +
                " JOIN role_premissions ON (role.id = role_premissions.roles_id)" +
                " JOIN premission ON (premission.id = role_premissions.premissions_id)";

        try {
            Query query2f = entityManager.createNativeQuery(sb);

            return new SelectionModel(RoleVM.class, query2f.getResultList(),
                    null, new Pagination(totalElements, page, size));
        } catch (Exception ex) {
            System.out.println("Error : getAllRoles()" + ex.getMessage());
        }

        return null;
    }
}
