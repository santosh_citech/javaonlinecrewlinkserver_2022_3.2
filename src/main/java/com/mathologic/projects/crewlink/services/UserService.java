package com.mathologic.projects.crewlink.services;

import com.mathologic.projects.crewlink.models.User;
import com.mathologic.projects.crewlink.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author : ADMIN 2/17/2022
 * @created : 17 / 02 / 2022 - 6:37 AM
 **/
@Service
public class UserService {

    @Autowired
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findUserByUserName(String userName) {
        return userRepository.findByUsername(userName);
    }
}
