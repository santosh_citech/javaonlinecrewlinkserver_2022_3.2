package com.mathologic.projects.crewlink.services.daoImpl;

import com.mathologic.projects.crewlink.models.Premission;
import com.mathologic.projects.crewlink.models.Role;
import com.mathologic.projects.crewlink.services.dao.PremissionService;
import org.hibernate.Session;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author : ADMIN 2/18/2022
 * @created : 18 / 02 / 2022 - 1:53 AM
 **/

@Service
@SuppressWarnings("unsafe")
public class PremissionServiceImpl implements PremissionService {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional(readOnly = false)
    @Override
    public void save(Premission premission) {
        try{
            Role role = entityManager.find(Role.class, 1L);
            Role role0 = entityManager.find(Role.class, 2L);
            Role role1 = entityManager.find(Role.class, 3L);
            HashSet<Role> set=new HashSet();
            set.add(role);
            set.add(role0);
            set.add(role1);

            premission.setRoles(set);
            HashSet<Premission> setp=new HashSet();
            setp.add(premission);
            role.setPremissions(setp);

           // premission.setRoles(set);
            entityManager.persist(role);
            entityManager.persist(premission);


        }catch(Exception e){
            System.out.println("Error : Premission save in PremissionServiceImpl "+e);
        }

    }

    @Transactional
    @Override
    public List<?> getAllPremission() {
        Session session  = entityManager.unwrap(Session.class);
        return (List<?>) session.createQuery("from Premission").list();
    }


    @Transactional(readOnly = false)
    @Override
    public List<Premission> findByAllParams(String sort, Long page, Long size) {

        Session session  = entityManager.unwrap(Session.class);
        Query query = session.createQuery("select p from Premission as p order by p.id desc");
        query.setFirstResult(page.intValue());
        query.setMaxResults(size.intValue());
        return (List<Premission>) query.getResultList();
    }
}
