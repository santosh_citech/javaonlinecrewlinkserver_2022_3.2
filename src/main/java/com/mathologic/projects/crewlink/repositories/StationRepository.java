package com.mathologic.projects.crewlink.repositories;

import com.mathologic.projects.crewlink.models.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface StationRepository extends JpaRepository<Station, Long> {
    Station findByCode(@Param("code")String code);
}