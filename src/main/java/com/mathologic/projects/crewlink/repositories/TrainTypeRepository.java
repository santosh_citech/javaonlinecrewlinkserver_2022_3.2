package com.mathologic.projects.crewlink.repositories;

import com.mathologic.projects.crewlink.models.TrainType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface TrainTypeRepository extends JpaRepository<TrainType, Long> {

    TrainType findByName(@Param("name")String name);
}