package com.mathologic.projects.crewlink.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


import com.mathologic.projects.crewlink.models.User;


@RepositoryRestResource
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsernameAndIsActive(@Param("Username") String username,
                                   @Param("IsActive") Boolean isActive);

    User findByActivationKey(@Param("activationKey") String activationKey);

    List<User> findByUsernameContains(@Param("Username") String username);

    User findByUsername(@Param("username") String username);

    @Query("SELECT u FROM User u WHERE u.username = :username")
     User getUserByUsername(@Param("username") String username);

//    @Query(value = "SELECT FROM user as u INNER JOIN role as r ON u.role_id = r.id")

    @Query(value = "Select ut from User as ut LEFT JOIN Role as rt "
            + "on (ut.id = rt.id)"
    )
    List<User> findAllByUser();

    @Query(value = "SELECT * " +
            " FROM User p LEFT JOIN Role j " +
            "ON p.id = j.id", nativeQuery = true)
    Page<User> findByAllParams(Pageable pageable);

}
