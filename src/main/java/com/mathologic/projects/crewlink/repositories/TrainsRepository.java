package com.mathologic.projects.crewlink.repositories;

import com.mathologic.projects.crewlink.models.Trains;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource
public interface TrainsRepository extends JpaRepository<Trains, Long> {

    Trains findByTrainNo(@Param("trainNo") Integer trainNo);

}
