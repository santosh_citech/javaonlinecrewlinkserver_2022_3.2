package com.mathologic.projects.crewlink.repositories;

import com.mathologic.projects.crewlink.models.Premission;
import com.mathologic.projects.crewlink.models.Station;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * @author : ADMIN 2/19/2022
 * @created : 19 / 02 / 2022 - 3:19 AM
 **/
public interface PremissionRepository  extends PagingAndSortingRepository<Premission, Long> {
    Page<Premission> findAll(Pageable pageable);
}
