package com.mathologic.projects.crewlink.dto;

import java.util.List;

/**
 * @author : ADMIN 2/18/2022
 * @created : 18 / 02 / 2022 - 8:06 PM
 **/
public class GenericDaoImpl implements GenericDao {
    @Override
    public Object find(Class entityClass, Object id) {
        return null;
    }

    @Override
    public void save(Object entity) {

    }

    @Override
    public Object update(Object entity) {
        return null;
    }

    @Override
    public void delete(Object entity) {

    }

    @Override
    public List findAll(Class entityClass) {
        return null;
    }
}
