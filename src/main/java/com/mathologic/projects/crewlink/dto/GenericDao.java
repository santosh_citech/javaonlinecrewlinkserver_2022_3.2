package com.mathologic.projects.crewlink.dto;

import java.util.List;

public interface GenericDao<T> {
    public T find(Class<T> entityClass, Object id);

    public void save(T entity);

    public T update(T entity);

    public void delete(T entity);

    public List<T> findAll(Class<T> entityClass);

}
