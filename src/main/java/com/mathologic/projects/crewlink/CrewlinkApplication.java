package com.mathologic.projects.crewlink;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {
		"com.mathologic.projects.crewlink",
		"com.mathologic.projects.crewlink.utility.converter",
		"com.mathologic.projects.crewlink.utility.library",
		"com.mathologic.projects.crewlink.utility.utils",
		"com.mathologic.projects.crewlink.services.dao",
		"com.mathologic.projects.crewlink.services.daoImpl",
		"com.mathologic.projects.crewlink.controllers",
		"com.mathologic.projects.crewlink.custom.controllers",
		"com.mathologic.projects.crewlink.custom.model",
		"com.mathologic.projects.crewlink.custom.repository",
		"com.mathologic.projects.crewlink.dto",
		"com.mathologic.projects.crewlink.exception",
		"com.mathologic.projects.crewlink.models",
		"com.mathologic.projects.crewlink.models.sub-user",
		"com.mathologic.projects.crewlink.models.selection",
		"com.mathologic.projects.crewlink.repositories",
		"com.mathologic.projects.crewlink.services",

})
@Configuration
@EnableAutoConfiguration
@SpringBootApplication
public class CrewlinkApplication extends SpringBootServletInitializer {
	private static final Logger logger = LoggerFactory.getLogger(CrewlinkApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(CrewlinkApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CrewlinkApplication.class);
	}
	@Bean
	CommandLineRunner init() {
		System.out.println("cmdrunner !");
		return null;
	}





}

