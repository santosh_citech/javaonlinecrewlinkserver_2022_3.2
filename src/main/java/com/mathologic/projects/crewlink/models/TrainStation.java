package com.mathologic.projects.crewlink.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mathologic.projects.crewlink.utility.utils.LocalDateTimeDeserializer;
import com.mathologic.projects.crewlink.utility.utils.LocalDateTimeSerializer;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
//@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "trainStation")
public class TrainStation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long trainNo;

    @Column
    private Long stopNo;



    @Column
    private String stationCode;

    @Column
    private Long arrivalDay;

    @Column
    private Long departureDay;


    @Basic
    // @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "arrival_time",columnDefinition = "TIME")
    private LocalTime arrivalTime;

//    @Convert(converter = LocalTimeAttributeConverter.class)

//    @Temporal(TemporalType.TIME)
    @Column(name = "departure_time",columnDefinition = "TIME")
    private LocalTime departureTime;

    @Column
    private Long arrivalInMinutes;

    @Column
    private Long departureInMinutes;

    @Column
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime arrivalDateTime;

    @Column
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime departureDateTime;

    @Column
    private Long distance;

    @Column
    private Long dayOfJourney;

    @Column
    private String LocoType;

    public String getLocoType() {
        return this.LocoType;
    }

    public void setLocoType(String LocoType) {
        this.LocoType = LocoType;
    }

    @Column(name = "journeyDuration", nullable = true)
    private Long journeyDuration;

    @Column(name = "markDelete", columnDefinition = "BIT(1) DEFAULT b'0'")
    private Boolean markDelete = false;

    @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;


    @ManyToOne(targetEntity=Trains.class,fetch = FetchType.LAZY)
    @JoinColumn(name = "train_id")
    private Trains trains;

    @ManyToOne(targetEntity=Station.class,fetch = FetchType.LAZY)
    @JoinColumn(name = "station_id")
    private Station stations;

    public Station getStations() {
        return stations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTrainNo() {
        return trainNo;
    }

    public void setTrainNo(Long trainNo) {
        this.trainNo = trainNo;
    }

    public Long getStopNo() {
        return stopNo;
    }

    public void setStopNo(Long stopNo) {
        this.stopNo = stopNo;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public Long getArrivalInMinutes() {
        return arrivalInMinutes;
    }

    public void setArrivalInMinutes(Long arrivalInMinutes) {
        this.arrivalInMinutes = arrivalInMinutes;
    }

    public Long getDepartureInMinutes() {
        return departureInMinutes;
    }

    public void setDepartureInMinutes(Long departureInMinutes) {
        this.departureInMinutes = departureInMinutes;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public Long getDayOfJourney() {
        return dayOfJourney;
    }

    public void setDayOfJourney(Long dayOfJourney) {
        this.dayOfJourney = dayOfJourney;
    }

    public Boolean getMarkDelete() {
        return markDelete;
    }

    public void setMarkDelete(Boolean markDelete) {
        this.markDelete = markDelete;
    }



    public Long getArrivalDay() {
        return arrivalDay;
    }

    public void setArrivalDay(Long arrivalDay) {
        this.arrivalDay = arrivalDay;
    }

    public Long getDepartureDay() {
        return departureDay;
    }

    public void setDepartureDay(Long departureDay) {
        this.departureDay = departureDay;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }


    public LocalTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public LocalTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalTime departureTime) {
        this.departureTime = departureTime;
    }

    public LocalDateTime getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(LocalDateTime arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public LocalDateTime getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(LocalDateTime departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public Long getJourneyDuration() {
        return journeyDuration;
    }

    public void setJourneyDuration(Long journeyDuration) {
        this.journeyDuration = journeyDuration;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        TrainStation that = (TrainStation) o;
//        return id.equals(that.id);
//    }
//
//    @Override
//    public int hashCode() {
//        return getClass().hashCode();
//    }
}
