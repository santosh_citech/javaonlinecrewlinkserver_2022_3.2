package com.mathologic.projects.crewlink.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mathologic.projects.crewlink.models.sub_user.UserEmail;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 25/9/15.
 * * ReCreated by santosh on 2/2/2022. on his birthday
 */
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@Table(name = "user",schema = "crewlink_db")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique = true)
    private String username;

    @Column
    private String password;


    @Column
    private String HashPassword;

    @Column(columnDefinition = "BIT(1) DEFAULT b'0'")
    private boolean markDelete;

    private String activationKey;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Role role;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<UserPlan> userPlans = new LinkedList<UserPlan>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(name = "FK_USER_TRAIN"))
    private List<Trains> trains = new LinkedList<Trains>();

    @Column(columnDefinition = "BIT(1) DEFAULT b'0'")
    private Boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "email_id")
    private UserEmail email_id;




    public List<UserPlan> getUserPlans() {
        return userPlans;
    }

    public void setUserPlans(List<UserPlan> userPlans) {
        this.userPlans = userPlans;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

   

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public Role getRole() {
        return role;
    }



    public List<Trains> getTrains() {
        return trains;
    }


    public void setTrains(List<Trains> trains) {
        this.trains = trains;
    }

    public void setRole(Role role) {
        this.role = role;
    }





    public boolean isMarkDelete() {
        return markDelete;
    }

    public void setMarkDelete(boolean markDelete) {
        this.markDelete = markDelete;
    }

    public String getHashPassword() {
        return HashPassword;
    }

    public void setHashPassword(String hashPassword) {
        HashPassword = hashPassword;
    }

    public UserEmail getEmail_id() {
        return email_id;
    }

    public void setEmail_id(UserEmail email_id) {
        this.email_id = email_id;
    }
}