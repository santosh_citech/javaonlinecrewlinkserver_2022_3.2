package com.mathologic.projects.crewlink.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *  create by santosh
 */
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "premission")
public class Premission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description ;
    private boolean canViewDashboard ;
    private boolean canCreateUsers;
    private boolean canViewUsers ;
    private boolean canUpdateUsers ;
    private boolean canDeleteUsers ;

    @Column(name = "created_at",nullable = false)
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at",nullable = false)
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @Column(name = "markDelete",columnDefinition = "BIT(1) DEFAULT b'0'")
    private Boolean markDelete = false;


    @ManyToMany(mappedBy = "premissions",fetch = FetchType.LAZY)
    private Set<Role> roles = new HashSet<>();

    public Premission() {
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public boolean isCanCreateUsers() {
        return canCreateUsers;
    }

    public void setCanCreateUsers(boolean canCreateUsers) {
        this.canCreateUsers = canCreateUsers;
    }

    public boolean isCanViewUsers() {
        return canViewUsers;
    }

    public void setCanViewUsers(boolean canViewUsers) {
        this.canViewUsers = canViewUsers;
    }

    public boolean isCanUpdateUsers() {
        return canUpdateUsers;
    }

    public void setCanUpdateUsers(boolean canUpdateUsers) {
        this.canUpdateUsers = canUpdateUsers;
    }

    public boolean isCanDeleteUsers() {
        return canDeleteUsers;
    }

    public void setCanDeleteUsers(boolean canDeleteUsers) {
        this.canDeleteUsers = canDeleteUsers;
    }


    public boolean isCanViewDashboard() {
        return canViewDashboard;
    }

    public void setCanViewDashboard(boolean canViewDashboard) {
        this.canViewDashboard = canViewDashboard;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getMarkDelete() {
        return markDelete;
    }

    public void setMarkDelete(Boolean markDelete) {
        this.markDelete = markDelete;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
