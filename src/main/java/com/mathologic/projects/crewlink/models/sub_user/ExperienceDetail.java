package com.mathologic.projects.crewlink.models.sub_user;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author : ADMIN 2/19/2022
 * @created : 19 / 02 / 2022 - 5:49 AM
 **/

@Entity
@Table(name = "ExperienceDetail")
public class ExperienceDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private LocalDate start_date;
    private LocalDate end_date;
    private String job_title;
    private String company_name;
    private String job_location_city;
    private String job_location_state;
    private String job_location_country;
    private String job_description;


    public LocalDate getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDate start_date) {
        this.start_date = start_date;
    }

    public LocalDate getEnd_date() {
        return end_date;
    }

    public void setEnd_date(LocalDate end_date) {
        this.end_date = end_date;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getJob_location_city() {
        return job_location_city;
    }

    public void setJob_location_city(String job_location_city) {
        this.job_location_city = job_location_city;
    }

    public String getJob_location_state() {
        return job_location_state;
    }

    public void setJob_location_state(String job_location_state) {
        this.job_location_state = job_location_state;
    }

    public String getJob_location_country() {
        return job_location_country;
    }

    public void setJob_location_country(String job_location_country) {
        this.job_location_country = job_location_country;
    }

    public String getJob_description() {
        return job_description;
    }

    public void setJob_description(String job_description) {
        this.job_description = job_description;
    }

}
    
     
     

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    

    
        
    
