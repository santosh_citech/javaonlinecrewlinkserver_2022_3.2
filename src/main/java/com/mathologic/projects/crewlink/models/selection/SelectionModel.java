package com.mathologic.projects.crewlink.models.selection;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : ADMIN 2/19/2022
 * @created : 19 / 02 / 2022 - 5:49 PM
 **/
@SuppressWarnings("unchecked")
public class SelectionModel implements Serializable {
    private static final long serialVersionUID = 1L;
    private Class<?> clazz;
    private List<Object[]> data;
    private Map<String, Integer> fields;
    private PaginationDetails paginationDetails;
    private Pagination pagination;

    private void fillFields() {

        fields = new HashMap<>();
        for (int i = 0; i < clazz.getDeclaredFields().length; i++) {
            fields.put(clazz.getDeclaredFields()[i].getName(), i);
        }
    }

    public SelectionModel(Class<?> clazz, List data, PaginationDetails paginationDetails, Pagination pagination) {
        super();
        this.clazz = clazz;
        this.data = data;
        this.paginationDetails = paginationDetails;
        this.pagination = pagination;
        this.fillFields();
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public List<Object[]> getData() {
        return data;
    }

    public void setData(List<Object[]> data) {
        this.data = data;
    }

    public Map<String, Integer> getFields() {
        return fields;
    }

    public void setFields(Map<String, Integer> fields) {
        this.fields = fields;
    }

    public PaginationDetails getPaginationDetails() {
        return paginationDetails;
    }

    public void setPaginationDetails(PaginationDetails paginationDetails) {
        this.paginationDetails = paginationDetails;
    }

    public Pagination getPagination() {
        return this.pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

}
