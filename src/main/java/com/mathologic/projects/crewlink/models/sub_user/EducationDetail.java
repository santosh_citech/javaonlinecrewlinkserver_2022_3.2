package com.mathologic.projects.crewlink.models.sub_user;

import java.time.LocalDateTime;

import javax.persistence.*;

/**
 * @author : ADMIN 2/19/2022
 * @created : 19 / 02 / 2022 - 5:40 AM
 **/

@Entity
@Table(name = "EducationDetail")
public class EducationDetail {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String certificate_degree_name;
    private String major;
    private String institute_university_name;
    private LocalDateTime start_date;
    private LocalDateTime completion_date;




    public String getCertificate_degree_name() {
        return certificate_degree_name;
    }

    public void setCertificate_degree_name(String certificate_degree_name) {
        this.certificate_degree_name = certificate_degree_name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getInstitute_university_name() {
        return institute_university_name;
    }

    public void setInstitute_university_name(String institute_university_name) {
        this.institute_university_name = institute_university_name;
    }

    public LocalDateTime getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDateTime start_date) {
        this.start_date = start_date;
    }

    public LocalDateTime getCompletion_date() {
        return completion_date;
    }

    public void setCompletion_date(LocalDateTime completion_date) {
        this.completion_date = completion_date;
    }
}
