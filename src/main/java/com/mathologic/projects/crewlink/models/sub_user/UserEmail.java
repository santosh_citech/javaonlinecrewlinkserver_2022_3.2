package com.mathologic.projects.crewlink.models.sub_user;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.mathologic.projects.crewlink.models.User;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "UserEmail",schema = "crewlink_db")
public class UserEmail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length =5)
    protected Long id;


    @Basic
    @Column(name = "name", nullable = false, length = 512,unique = true)
    @NotNull
    protected String name;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdAt;

    @Column
    protected String realName;


    @Column(nullable = false, columnDefinition = "TINYINT(1) DEFAULT b'0'")
    protected boolean isActive = true;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true,mappedBy = "email_id")
    private Set<User> users = new HashSet<>();

    public Date getCreatedAt() {
        return this.createdAt;
    }


    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @PrePersist
    protected void onCreate() {
        this.createdAt = new Date();
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}