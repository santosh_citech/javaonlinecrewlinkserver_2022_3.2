package com.mathologic.projects.crewlink.models;

import com.mathologic.projects.crewlink.enums.Days;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "train")
public class Trains implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private Integer trainNo;

    // Monday, Tuesday,Wednesday,Thursday SaturDay, etc.
    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private Days startDay;



    private String trainName;

    private String fromStation;

    private String toStation;

    private String trainType;



    @ManyToOne(targetEntity=Station.class,fetch = FetchType.LAZY)
    @JoinColumn(name = "sourceId")
    private Station sourceStation;

    @ManyToOne(targetEntity=Station.class,fetch = FetchType.LAZY)
    @JoinColumn(name = "destinationId")
    private Station destinationStation;

    @ManyToOne(targetEntity=TrainType.class,fetch = FetchType.LAZY)
    @JoinColumn(name="trainTypeId")
    private TrainType traintype;

    @ManyToMany(mappedBy="trains", fetch = FetchType.LAZY)
    private List<User> users = new LinkedList<User>();


    @Column(name = "created_at",nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at",nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;


    @Column(name = "markDelete",columnDefinition = "BIT(1) DEFAULT b'0'")
    private Boolean markDelete = false;

    @OneToMany(mappedBy = "trains", cascade = CascadeType.REMOVE,fetch = FetchType.LAZY)
    private List<TrainStation> trainStations = new LinkedList<TrainStation>();

    public Days getStartDay() {
        return startDay;
    }

    public void setStartDay(Days startDay) {
        this.startDay = startDay;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTrainNo() {
        return trainNo;
    }

    public void setTrainNo(Integer trainNo) {
        this.trainNo = trainNo;
    }





    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getFromStation() {
        return fromStation;
    }

    public void setFromStation(String fromStation) {
        this.fromStation = fromStation;
    }

    public String getToStation() {
        return toStation;
    }

    public void setToStation(String toStation) {
        this.toStation = toStation;
    }

    public String getTrainType() {
        return trainType;
    }

    public void setTrainType(String trainType) {
        this.trainType = trainType;
    }

    public Station getSourceStation() {
        return sourceStation;
    }

    public void setSourceStation(Station sourceStation) {
        this.sourceStation = sourceStation;
    }

    public Station getDestinationStation() {
        return destinationStation;
    }

    public void setDestinationStation(Station destinationStation) {
        this.destinationStation = destinationStation;
    }

    public TrainType getTraintype() {
        return traintype;
    }



    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getMarkDelete() {
        return markDelete;
    }

    public void setMarkDelete(Boolean markDelete) {
        this.markDelete = markDelete;
    }

    public void setTraintype(TrainType traintype) {
        this.traintype = traintype;
    }

    public List<TrainStation> getTrainStations() {
        return trainStations;
    }

    public void setTrainStations(List<TrainStation> trainStations) {
        this.trainStations = trainStations;
    }
}
