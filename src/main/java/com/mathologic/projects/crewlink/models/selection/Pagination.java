package com.mathologic.projects.crewlink.models.selection;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : ADMIN 2/20/2022
 * @created : 20 / 02 / 2022 - 12:30 AM
 **/
public class Pagination {
    private Long totalItems;
    private Long currentPage;
    private Long pageSize;
    private Long totalPages;
    private Long startPage;
    private Long endPage;
    private Long startIndex;
    private Long endIndex;
    private List<Long> pages = new ArrayList<>();

    public Pagination() {
        super();
    }


    public Pagination(Long totalItems, Long currentPage, Long pageSize) {
        this.totalItems = totalItems;
        this.currentPage = currentPage;
        this.pageSize = pageSize;

        totalPages = totalItems / pageSize;

        if (totalPages <= 5) {
            startPage = 1L;
            endPage = totalPages;
        } else {
            if (currentPage <= 3) {
                startPage = 1L;
                endPage = 5L;
            } else if (currentPage + 1 >= totalPages) {
                startPage = totalPages - 4;
                endPage = totalPages;
            } else {

                if ((totalPages - (currentPage - 2)) == 5) {
                    startPage = currentPage - 1;
                    endPage = currentPage + 3;
                } else {
                    startPage = currentPage - 2;
                    endPage = currentPage + 2;
                }
            }
        }
        startIndex = (currentPage - 1) * pageSize;
        endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        for(long i = startPage;i< endPage + 1;i++){
            pages.add(i);

        }
    }



    public Long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Long totalItems) {
        this.totalItems = totalItems;
    }

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Long totalPages) {
        this.totalPages = totalPages;
    }

    public Long getStartPage() {
        return startPage;
    }

    public void setStartPage(Long startPage) {
        this.startPage = startPage;
    }

    public Long getEndPage() {
        return endPage;
    }

    public void setEndPage(Long endPage) {
        this.endPage = endPage;
    }

    public Long getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Long startIndex) {
        this.startIndex = startIndex;
    }

    public Long getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(Long endIndex) {
        this.endIndex = endIndex;
    }

    public List getPages() {
        return pages;
    }

    public void setPages(List pages) {
        this.pages = pages;
    }
}
