package com.mathologic.projects.crewlink.models.sub_user;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : ADMIN 2/19/2022
 * @created : 19 / 02 / 2022 - 5:15 AM
 **/

@Entity
@Table(name = "UserProfile")
public class UserProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotNull
    private String firstname;
    private String lastname;

    private String mobilenumber;

    UserProfile() {

    }



    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }
}
