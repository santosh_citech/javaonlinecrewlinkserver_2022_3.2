package com.mathologic.projects.crewlink.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "station")
public class Station {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;
    private String name;
    
    @Column(columnDefinition = "BIGINT DEFAULT 30")
    private Long headStationSignOnTime = 30L;

    @Column(columnDefinition = "BIGINT DEFAULT 30")
    private Long headStationSignOffTime = 30L;
    
    @Column(columnDefinition = "BIGINT DEFAULT 1")
    private Long noOfBeds;

    @Column(columnDefinition = "BIGINT DEFAULT 30")
    private Long outStationSignOnTime = 30L;
    
    @Column(columnDefinition = "BIGINT DEFAULT 30")
    private Long outStationSignOffTime = 30L;

    @Column(columnDefinition = "tinyint(1) default 1")
    private boolean markDelete = true;

    @Column(name = "created_at",nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at",nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;


    @OneToMany(mappedBy = "sourceStation", cascade = CascadeType.REMOVE,fetch = FetchType.LAZY)
    private List<Trains> fromTrains = new LinkedList<Trains>();

    @OneToMany(mappedBy = "destinationStation", cascade = CascadeType.REMOVE,fetch = FetchType.LAZY)
    private List<Trains> toTrains = new LinkedList<Trains>();

    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "stations"
    )
    private List<TrainStation> trainstations = new ArrayList<TrainStation>();

    public Station(){

    }

    Station(
            String code,
            String name,
            Long headStationSignOnTime,
            Long headStationSignOffTime,
            Long noOfBeds,
            Long outStationSignOnTime,
            Long outStationSignOffTime,
            boolean markDelete

    ) {
        this.code = code;
        this.name = name;
        this.headStationSignOnTime = headStationSignOnTime;
        this.headStationSignOffTime = headStationSignOffTime;
        this.outStationSignOnTime = outStationSignOnTime;
        this.outStationSignOffTime = outStationSignOffTime;
        this.markDelete = markDelete;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOutStationSignOnTime() {
        return outStationSignOnTime;
    }

    public void setOutStationSignOnTime(Long outStationSignOnTime) {
        this.outStationSignOnTime = outStationSignOnTime;
    }

    public Long getOutStationSignOffTime() {
        return outStationSignOffTime;
    }

    public void setOutStationSignOffTime(Long outStationSignOffTime) {
        this.outStationSignOffTime = outStationSignOffTime;
    }

    public boolean isMarkDelete() {
        return markDelete;
    }

    public void setMarkDelete(boolean markDelete) {
        this.markDelete = markDelete;
    }

    public Long getHeadStationSignOnTime() {
        return headStationSignOnTime;
    }

    public void setHeadStationSignOnTime(Long headStationSignOnTime) {
        this.headStationSignOnTime = headStationSignOnTime;
    }

    public Long getHeadStationSignOffTime() {
        return headStationSignOffTime;
    }

    public void setHeadStationSignOffTime(Long headStationSignOffTime) {
        this.headStationSignOffTime = headStationSignOffTime;
    }

    public Long getNoOfBeds() {
        return noOfBeds;
    }

    public List<Trains> getFromTrains() {
        return fromTrains;
    }

    public void setFromTrains(List<Trains> fromTrains) {
        this.fromTrains = fromTrains;
    }

    public List<Trains> getToTrains() {
        return toTrains;
    }

    public void setToTrains(List<Trains> toTrains) {
        this.toTrains = toTrains;
    }

    public void setNoOfBeds(Long noOfBeds) {
        this.noOfBeds = noOfBeds;
    }


    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<TrainStation> getTrainstations() {
        return trainstations;
    }

    public void setTrainstations(List<TrainStation> trainstations) {
        this.trainstations = trainstations;
    }
}
