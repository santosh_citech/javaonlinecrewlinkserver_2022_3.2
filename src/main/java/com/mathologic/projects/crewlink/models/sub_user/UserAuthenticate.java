package com.mathologic.projects.crewlink.models.sub_user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * @author : ADMIN 2/19/2022
 * @created : 19 / 02 / 2022 - 6:25 AM
 **/

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@Table(name = "UserAuthenticate",schema = "crewlink_db")
public class UserAuthenticate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(nullable = false, columnDefinition = "TINYINT(1) DEFAULT b'0'")
    private boolean account_disabled;
    @Column(nullable = false, columnDefinition = "TINYINT(1) DEFAULT b'0'")
    private boolean account_expired;
    @Column(nullable = false, columnDefinition = "TINYINT(1) DEFAULT b'0'")
    private boolean account_locked;
    @Column(nullable = false, columnDefinition = "TINYINT(1) DEFAULT b'0'")
    private boolean credentials_expired;


    @Column(name = "created_at",nullable = false)
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at",nullable = false)
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;



    public boolean isAccount_disabled() {
        return account_disabled;
    }

    public void setAccount_disabled(boolean account_disabled) {
        this.account_disabled = account_disabled;
    }

    public boolean isAccount_expired() {
        return account_expired;
    }

    public void setAccount_expired(boolean account_expired) {
        this.account_expired = account_expired;
    }

    public boolean isAccount_locked() {
        return account_locked;
    }

    public void setAccount_locked(boolean account_locked) {
        this.account_locked = account_locked;
    }

    public boolean isCredentials_expired() {
        return credentials_expired;
    }

    public void setCredentials_expired(boolean credentials_expired) {
        this.credentials_expired = credentials_expired;
    }
}
