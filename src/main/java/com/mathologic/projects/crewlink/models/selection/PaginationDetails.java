package com.mathologic.projects.crewlink.models.selection;

import java.io.Serializable;

/**
 * @author : ADMIN 2/19/2022
 * @created : 19 / 02 / 2022 - 7:27 PM
 **/
public class PaginationDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long totalElements;
    private Long startIndex;
    private Long currentPage;
    private Long totalPages;
    private String baseItemRestUri;
    private Long totalDocs;
    private Long limit;
    private Long page;
    private Long nextPage;
    private Long prevPage;
    private Boolean hasNextPage;
    private Boolean hasPrevPage;


    public PaginationDetails(Long totalElements, Long startIndex,
            Long currentPage, Long totalPages, String baseItemRestUri) {
        super();
        this.totalElements = totalElements;
        this.startIndex = startIndex;
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.baseItemRestUri = baseItemRestUri;
    }

    public PaginationDetails(Long totalElements, Long startIndex, Long currentPage, Long totalPages,
            String baseItemRestUri, Long totalDocs, Long limit, Long page, Long nextPage, Long prevPage,
            Boolean hasNextPage, Boolean hasPrevPage) {
        super();
        this.totalElements = totalElements;
        this.startIndex = startIndex;
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.baseItemRestUri = baseItemRestUri;
        this.totalDocs = totalDocs;
        this.limit = limit;
        this.page = page;
        this.nextPage = nextPage;
        this.prevPage = prevPage;
        this.hasNextPage = hasNextPage;
        this.hasPrevPage = hasPrevPage;

    }

    public Long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Long totalElements) {
        this.totalElements = totalElements;
    }

    public Long getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Long startIndex) {
        this.startIndex = startIndex;
    }

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public Long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Long totalPages) {
        this.totalPages = totalPages;
    }

    public String getBaseItemRestUri() {
        return baseItemRestUri;
    }

    public void setBaseItemRestUri(String baseItemRestUri) {
        this.baseItemRestUri = baseItemRestUri;
    }

    public Long getTotalDocs() {
        return totalDocs;
    }

    public void setTotalDocs(Long totalDocs) {
        this.totalDocs = totalDocs;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public Long getNextPage() {
        return nextPage;
    }

    public void setNextPage(Long nextPage) {
        this.nextPage = nextPage;
    }

    public Long getPrevPage() {
        return prevPage;
    }

    public void setPrevPage(Long prevPage) {
        this.prevPage = prevPage;
    }

    public Boolean getHasNextPage() {
        return hasNextPage;
    }

    public void setHasNextPage(Boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    public Boolean getHasPrevPage() {
        return hasPrevPage;
    }

    public void setHasPrevPage(Boolean hasPrevPage) {
        this.hasPrevPage = hasPrevPage;
    }


}
