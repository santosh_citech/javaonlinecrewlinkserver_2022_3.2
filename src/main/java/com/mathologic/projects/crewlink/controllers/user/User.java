package com.mathologic.projects.crewlink.controllers.user;

import com.mathologic.projects.crewlink.custom.model.UserVM;
import com.mathologic.projects.crewlink.models.Role;
import com.mathologic.projects.crewlink.repositories.RoleRepository;
import com.mathologic.projects.crewlink.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*",allowedHeaders = "*")
@Controller
@RequestMapping("/api/v1/user")
public class User {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST, headers = {"Content-type=application/json"})
    @ResponseBody
    public String saveUser(@RequestBody UserVM model) {
        try{
            if(model!=null){
                System.out.println(model.getFirstName());
                com.mathologic.projects.crewlink.models.User newUser = new com.mathologic.projects.crewlink.models.User();
//                Role r = roleRepository.findByName(model.getRolecode());
//                newUser.setRole(r);
                newUser.setIsActive(model.getIsActive());

                newUser.setActivationKey(model.getActivationKey());


                newUser.setUsername(model.getUsername());
                newUser.setIsActive(true);
                newUser.setActivationKey("MIIEowIBAAKCAQEArlyL6RmqQwFrcU8dOTmWsM/IjqefbPdaTl3dhQe7BqIib0Xg");

                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                System.out.println("Encoded password is " + passwordEncoder.encode(model.getPassword()));
                newUser.setPassword(model.getPassword());

                userRepository.save(newUser);
                return "User saved successfully !!!";

            }
        }catch (Exception exe){
            System.out.println("Error in saveUser()"+ exe.getMessage()+"\n"+ exe.getStackTrace());
        }
        return "Error in saving  successfully";
    }

    @RequestMapping(value = "/getALLUsers", method = RequestMethod.GET, headers = {"Content-type=application/json"})
    @ResponseBody
    public Page<com.mathologic.projects.crewlink.models.User> getALLUsers(
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false, defaultValue = "0") Long page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Long size

    )
    {
        try {

            return userRepository.findByAllParams(PageRequest.of(0, 10));

        }catch (Exception e)
        {
            System.out.println("Error in  getAllUser()"+e.getMessage()+"\n"+e.getStackTrace());
        }

        return null;
    }
}
