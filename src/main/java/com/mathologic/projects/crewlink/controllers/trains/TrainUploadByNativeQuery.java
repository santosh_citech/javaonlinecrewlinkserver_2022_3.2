package com.mathologic.projects.crewlink.controllers.trains;

import com.mathologic.projects.crewlink.enums.Days;
import com.mathologic.projects.crewlink.models.Trains;
import com.mathologic.projects.crewlink.services.dao.TrainVMRepository;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;


@Controller
public class TrainUploadByNativeQuery {

    private static final Logger log = LoggerFactory.getLogger(TrainUploadByNativeQuery.class);
    @Autowired(required=true)
    private TrainVMRepository trainVMRepository;
    @RequestMapping(value = "/TrainUploadByNativeQuery", method = RequestMethod.POST)
    public @ResponseBody String TrainUpload(@RequestParam("file") MultipartFile file,
            HttpServletRequest request) {

        if(file.isEmpty()){
            return "Please select a file to upload";
        }
        if(!file.getOriginalFilename().contains(".csv")){
            return "please upload csv file";
        }

        if(!file.isEmpty()){
            try {
                String[] columns;
                Reader reader = new InputStreamReader(file.getInputStream());
                CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

                while ((columns = csvReader.readNext()) != null) {
                    processRecords(columns);
                }
            } catch (Exception e) {
                log.error("Exception in TrainUploadByNativeQuery() " + e.getStackTrace().toString());
            }

        }


        return "Uploaded successfully !!!";
    }

    public void processRecords(String[] columns) {
        List<Trains> trainsList = new LinkedList<Trains>();
        for(int x = 4;x<=10;x++) {
            if (!columns[x].isEmpty()) {
                Trains train  = new Trains();
                    train.setTrainNo(Integer.parseInt(columns[0]));
                    train.setTrainName(columns[1]);
                    train.setFromStation(columns[2]);
                    train.setToStation(columns[3]);
                    train.setTrainType(columns[11]);
                    train.setStartDay(Days.values()[x-4]);
                trainsList.add(train);
            }

        }
        trainVMRepository.saveTrainUsingNative(trainsList);
    }
}
