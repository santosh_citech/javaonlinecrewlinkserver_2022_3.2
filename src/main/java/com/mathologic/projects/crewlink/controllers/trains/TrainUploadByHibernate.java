package com.mathologic.projects.crewlink.controllers.trains;

import com.mathologic.projects.crewlink.enums.Days;
import com.mathologic.projects.crewlink.models.Station;
import com.mathologic.projects.crewlink.models.TrainType;
import com.mathologic.projects.crewlink.models.Trains;
import com.mathologic.projects.crewlink.repositories.StationRepository;
import com.mathologic.projects.crewlink.repositories.TrainTypeRepository;
import com.mathologic.projects.crewlink.repositories.TrainsRepository;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

@Controller
public class TrainUploadByHibernate {

    @Autowired(required=true)
    private StationRepository stationRepo;

    @Autowired(required=true)
    private TrainsRepository trainsRepository;

    @Autowired(required=true)
    private TrainTypeRepository trainTypeRepository;

    private static final Logger log = LoggerFactory.getLogger(TrainUploadByHibernate.class);

    @RequestMapping(value = "/TrainUploadByHibernate", method = RequestMethod.POST)
    public @ResponseBody String TrainUpload(@RequestParam("file") MultipartFile file,
            HttpServletRequest request) {

        try {
            String[] nextLine = null;
            Reader reader = new InputStreamReader(file.getInputStream());
            CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

            while ((nextLine = csvReader.readNext()) != null) {
                processRecords(nextLine);
            }
        } catch (Exception e) {
            log.error("Exception in TrainUploadByHibernate() " + e.getStackTrace());
        }

        return "Uploaded successfully !!!";
    }

    public void processRecords(String[] columns) {

        TrainType trainType = null;
        Station sourceStation = null;
        Station destinationStation = null;
        try {
            sourceStation = stationRepo.findByCode(columns[2]);
            if (sourceStation == null) {
                sourceStation = new Station();
                sourceStation.setCode(columns[2]);
            }
            stationRepo.save(sourceStation);

        } catch (Exception ex) {
            log.error("Exception in processRecords() in sourceStation " + ex.getStackTrace());
        }
        try {
            destinationStation = stationRepo.findByCode(columns[3]);
            if (destinationStation == null) {
                destinationStation = new Station();
                destinationStation.setCode(columns[3]);
            }
            stationRepo.save(destinationStation);

        } catch (Exception ex) {

            log.error("Exception in processRecords() in destinationStation " + ex.getStackTrace());
        }

        try {
            trainType = trainTypeRepository.findByName(columns[11]);
            if (trainType == null) {
                trainType = new TrainType();
                trainType.setName(columns[11]);
            }
            trainTypeRepository.save(trainType);

        } catch (Exception ex) {

            log.error("Exception in processRecords() in trainTypeRepository " + ex.getStackTrace());
        }

        try {
            List<Trains> trains = new LinkedList<Trains>();
            for (int i = 4; i <=10; i++) {
                if (!columns[i].isEmpty()) {
                    Days startDay = Days.values()[i - 4];
                    Trains train = trainsRepository.findByTrainNo(Integer.parseInt(columns[0]));
                    if (train == null) {
                        train = new Trains();
                        train.setTrainNo(Integer.parseInt(columns[0]));
                        train.setStartDay(startDay);
    
                    }

                    train.setTrainName(columns[1]);
                    train.setSourceStation(sourceStation);
                    train.setDestinationStation(destinationStation);
                    train.setTraintype(trainType);
                    train.setToStation(columns[2]);
                    train.setFromStation(columns[3]);
                    train.setTrainType(columns[11]);
                    trains.add(train);
                }
            }

            trainsRepository.saveAll(trains);

        } catch (Exception exe) {
            System.out.println("Error : in Train : " + exe.getMessage());
            log.error("Exception in processRecords() in train saving  " + exe.getStackTrace());
        }

    }
}
