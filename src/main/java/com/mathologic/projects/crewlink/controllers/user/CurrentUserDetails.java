package com.mathologic.projects.crewlink.controllers.user;

import java.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.mathologic.projects.crewlink.models.Role;

import com.mathologic.projects.crewlink.models.User;
import com.mathologic.projects.crewlink.repositories.UserRepository;
import com.mathologic.projects.crewlink.repositories.RoleRepository;

/**
 * Created by vivek on 31/10/15.
 * ReCreated by santosh on 2/2/2022. on his birthday
 */
@Controller
@RequestMapping("/api/custom/user")
public class CurrentUserDetails {
    @Autowired
     UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @RequestMapping(value="/time",method=RequestMethod.GET)
    public @ResponseBody LocalTime getTime(@RequestParam("time")@DateTimeFormat(pattern="HH:mm")LocalTime time){

        return time;
    }


    @RequestMapping(value="/getTimes",method=RequestMethod.GET)
    public @ResponseBody String getTimes(){
        String s = "santosh";
        return "hi";
    }

    @RequestMapping(value="/saveUser",method=RequestMethod.POST)
    public @ResponseBody String saveUser(){
        User newUser = new User();
//        Role r = roleRepository.findByName("SUPERUSER");
//        newUser.setRole(r);
        newUser.setIsActive(true);
        newUser.setActivationKey("jkjkjk5522");

        newUser.setUsername("yuhhh");

        userRepository.save(newUser);
        return "User saved successfully !!!";
    }

    @RequestMapping(value="/activate/{activationKey}", method= RequestMethod.GET)
    public @ResponseBody Boolean activateUser(@PathVariable("activationKey") String activationKey) {
        if(activationKey!=null && activationKey!="") {
            User user = userRepository.findByActivationKey(activationKey);
            if(user!=null) {
                user.setIsActive(true);
                userRepository.save(user);
                return true;
            }
        }
        return false;
    }

}
