package com.mathologic.projects.crewlink.controllers;

import com.mathologic.projects.crewlink.services.dao.TrainStationVMDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
public class TrainStationController {
    private static final Logger logger = LoggerFactory.getLogger(TrainStationController.class);
    @Autowired(required=true)
    TrainStationVMDao trainStationVMDao;

    @RequestMapping(value = "/listStations", method = RequestMethod.GET, headers = {"Content-type=application/json"})
    @ResponseBody
    public List<Map<Object, Object>> listStations(
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false, defaultValue = "0") Long page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Long size

    )
    {
        try {
            List<Map<Object, Object>> result =  trainStationVMDao.list(sort,null,page,size);
            return result;
        }catch (Exception e) {
            System.out.println("Error in  listStations()"+e.getMessage());
            logger.error("Error in  listStations()"+e.getMessage());
        }

        return null;
    }
}
