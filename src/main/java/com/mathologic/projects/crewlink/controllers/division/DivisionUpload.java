package com.mathologic.projects.crewlink.controllers.division;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

@Controller
public class DivisionUpload {

    @Autowired(required=true)
    private EntityManagerFactory emf;

    @RequestMapping(value = "/uploadDivision", method = RequestMethod.POST)
    public @ResponseBody String uploadDivision(@RequestParam("file") MultipartFile file,
            HttpServletRequest request) throws IOException {

        byte[] bytes = file.getBytes();
        ByteArrayInputStream inputFilestream = new ByteArrayInputStream(bytes);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputFilestream));
        String line = "";
        boolean skipFirstLine = true;
        while ((line = br.readLine()) != null) {
            if (!line.isEmpty()) {
                if (skipFirstLine) { // skip data header
                    skipFirstLine = false;
                    continue;
                } else {
                    String[] data = line.split(",");
                    saveToDB(data[1]);

                }

            }
        }
        br.close();

        return "Uploaded Successfully!!!";

    }

    void saveToDB(String name) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        em.createNativeQuery("INSERT INTO division (name) VALUES (?)")
                .setParameter(1, name)
                .executeUpdate();
        tx.commit();
        em.close();

    }

}
