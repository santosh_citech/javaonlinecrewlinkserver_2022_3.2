package com.mathologic.projects.crewlink.controllers;

import com.mathologic.projects.crewlink.models.Premission;
import com.mathologic.projects.crewlink.services.dao.PremissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author : ADMIN 2/18/2022
 * @created : 18 / 02 / 2022 - 3:30 AM
 **/

@CrossOrigin(origins = "*",allowedHeaders = "*")
@Controller
@RequestMapping("/api/v1/premission")
public class PremissionController {

    @Autowired(required=true)
    PremissionService premissionService;


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/save", method = RequestMethod.POST, headers = {"Content-type=application/json"})
    @ResponseBody
    public String save() {
        try{
            Premission pres = null;
            if(pres==null){
                pres = new Premission();
                pres.setCanCreateUsers(true);
                pres.setCanDeleteUsers(true);
                pres.setCanUpdateUsers(true);
                pres.setCanViewDashboard(true);
                pres.setDescription("This user can do all the nessary premission");


                premissionService.save(pres);

                return "Premission saved successfully !!!";
            }
        }catch (Exception exe){
            System.out.println("Error in saveUser()"+ exe.getMessage()+"\n"+ exe.getStackTrace());
        }
        return "Error in saving  successfully";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
//    @PostAuthorize("filterObject.email == authentication.principal.email")
    @RequestMapping(value = "/getAllPremission", method = RequestMethod.GET, headers = {"Content-type=application/json"})
    @ResponseBody
    public List<Premission> getAllPremission(
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false, defaultValue = "0") Long page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Long size

    )
    {
        try {
            return premissionService.findByAllParams(sort,page,size);
        }catch (Exception e)
        {
            System.out.println("Error: getAllPremission() "+e.getMessage()+"\n"+e.getStackTrace());
        }

        return null;
    }
}




