package com.mathologic.projects.crewlink.controllers.trains;

import com.mathologic.projects.crewlink.models.selection.SelectionModel;
import com.mathologic.projects.crewlink.services.dao.TrainVMRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
public class TrainController {
    private static final Logger logger = LoggerFactory.getLogger(TrainController.class);

    @Autowired(required = false)
    TrainVMRepository trainVMRepository;

//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/listTrains", method = RequestMethod.GET, headers = {"Content-type=application/json"})
    @ResponseBody
    public List<Map<Object, Object>> listTrains(
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false, defaultValue = "0") Long page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Long size

    )
    {
        try {
            List<Map<Object, Object>> result =  trainVMRepository.listTrains(sort,null,page,size);
            return result;
        }catch (Exception e) {
            System.out.println("Error in  listTrains()"+e.getMessage());
            logger.error("Error in  listTrains()"+e.getMessage());
        }

        return null;
    }


//  /  @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/listAllTrains", method = RequestMethod.GET)
    @ResponseBody
    public SelectionModel listAllTrains(
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false, defaultValue = "0") Long page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Long size

    )
    {
        try {

            return  trainVMRepository.listALLTrains(sort,page,size);
        }catch (Exception e) {
            System.out.println("Error in  listAllTrains()"+e.getMessage());
            logger.error("Error in  listAllTrains()"+e.getMessage());
        }

        return null;
    }



}
