package com.mathologic.projects.crewlink.controllers.uploads;

import com.mathologic.projects.crewlink.utility.converter.csvToDataBase;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStreamReader;
import java.io.Reader;

@Controller
@SuppressWarnings("unused")
public class TrainStationUpload {

    private static final Logger logger = LoggerFactory.getLogger(TrainStationUpload.class);
    @Autowired(required=true)
    @Qualifier("TrainTimeTable")
    private csvToDataBase trainTimeTableCsvToDatabase;
    @RequestMapping(value = "/TrainStationUpload", method = RequestMethod.POST)
    public @ResponseBody
    String TrainTimeTableUpload(@RequestParam("file") MultipartFile file,
                       HttpServletRequest request) {

        if (file.isEmpty()) {
            return "Please select a file to upload";
        }
        if (!file.getOriginalFilename().contains(".csv")) {
            return "please upload csv file";
        }

        if (!file.isEmpty()) {
            try {
                String[] columns;
                Reader reader = new InputStreamReader(file.getInputStream());
                CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

                while ((columns = csvReader.readNext()) != null) {
                    trainTimeTableCsvToDatabase.processRecords(columns);

                }
            } catch (Exception e) {
                logger.error("Exception in TrainUploadByNativeQuery() " + e.getStackTrace().toString());
                System.out.println("Exception in TrainUploadByNativeQuery() " + e.getMessage());
            }

        }


        return "Uploaded successfully !!!";
    }
}
