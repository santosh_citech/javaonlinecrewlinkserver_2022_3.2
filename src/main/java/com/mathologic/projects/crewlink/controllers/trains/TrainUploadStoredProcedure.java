package com.mathologic.projects.crewlink.controllers.trains;

import com.mathologic.projects.crewlink.services.dao.TrainVMRepository;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStreamReader;
import java.io.Reader;

@Controller
public class TrainUploadStoredProcedure {


    @Autowired(required=true)
    TrainVMRepository trainVMRepository;

    private static final Logger log = LoggerFactory.getLogger(TrainUploadStoredProcedure.class);

    @RequestMapping(value = "/TrainUploadStoredProcedure",headers = "Content-Type= multipart/form-data" ,method = RequestMethod.POST)
    public @ResponseBody String TrainUploadStoredProcedure(@RequestParam("file") MultipartFile file,
            HttpServletRequest request) {

        try {
            String[] nextLine = null;
            Reader reader = new InputStreamReader(file.getInputStream());
            CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

            while ((nextLine = csvReader.readNext()) != null) {
                processRecords(nextLine);
            }
        } catch (Exception e) {
            log.error("Exception in TrainUpload()", e.getMessage() + "" + e.getStackTrace());
        }

        return "Uploaded successfully !!!";
    }

    private Boolean processRecords(String[] columns) {

        Integer trainNo = null;
        String startDays = new String();
        Integer day = 0;
        try {
            trainNo =Integer.parseInt(columns[0]);
            for (int i = 4; i <= 10; i++) {
                if (!columns[i].isEmpty()) {
                    day = i - 4;
                    startDays = startDays.concat("," + day.toString());
                }

            }
            startDays = startDays.substring(1, startDays.length());
            trainVMRepository.saveTrainStoredProcedure(trainNo,
                    columns[1], columns[2],
                    columns[3], startDays,
                    columns[11]);

        } catch (Exception exe) {
            System.out.println(exe.getStackTrace());
        }

        return false;
    }

}
