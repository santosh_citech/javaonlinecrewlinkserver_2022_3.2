package com.mathologic.projects.crewlink.controllers;

import com.mathologic.projects.crewlink.enums.RoleEnum;
import com.mathologic.projects.crewlink.exception.CrewLinkException;
import com.mathologic.projects.crewlink.models.Role;
import com.mathologic.projects.crewlink.models.selection.SelectionModel;
import com.mathologic.projects.crewlink.repositories.RoleRepository;
import com.mathologic.projects.crewlink.services.dao.IRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RoleController {

    @Autowired(required = false)
    IRole roleService;

    @Autowired(required = false)
    RoleRepository roleRepository;


    @RequestMapping(value = "/SaveRole", method = RequestMethod.GET)
    public @ResponseBody
    String SaveRole() throws CrewLinkException {

        Role role = new Role();
        role.setRolename(RoleEnum.EMPLOYEE);
        roleService.saveInDb(role);
        return "Role saved";
    }

    @RequestMapping(value = "/listAllRoles", method = RequestMethod.GET, headers = {"Content-type=application/json"})
    public @ResponseBody
    SelectionModel listAllRoles(
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false, defaultValue = "0") Long page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Long size
    ) throws CrewLinkException {


        return roleService.getAllRoles(sort, size, page);

    }

    private Pageable createPageRequest(Long page, Long size, String sort) {
        return PageRequest.of(page.intValue(),
                size.intValue());

    }

    @RequestMapping(value = "/findByAllRoles", method = RequestMethod.GET, headers = {"Content-type=application/json"})
    public @ResponseBody
    org.springframework.data.domain.Page<Role> findByAllRoles(
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "page", required = false, defaultValue = "0") Long page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Long size,
            @RequestParam(value = "name", required = false) String name
    ) {
        try {
            return roleRepository.findAll(createPageRequest(page, size, sort));
        } catch (Exception ex) {
            System.out.println("Error: findByAllRoles() "+ex);
        }
        return null;
    }

}
