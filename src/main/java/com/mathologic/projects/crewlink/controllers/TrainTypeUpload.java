package com.mathologic.projects.crewlink.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class TrainTypeUpload {



    @Autowired(required=true)
    private EntityManagerFactory emf;

    @RequestMapping(value = "/uploadTrainType", method = RequestMethod.POST)
    public @ResponseBody String uploadTrainType(@RequestParam("file") MultipartFile TrainTypeFile,
            HttpServletRequest request) throws IOException {

        byte[] bytes = TrainTypeFile.getBytes();
        ByteArrayInputStream inputFilestream = new ByteArrayInputStream(bytes);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputFilestream));
        String line = "";
        boolean skipFirstLine = true;
        while ((line = br.readLine()) != null) {
            if (!line.isEmpty()) {
                if (skipFirstLine) { // skip data header
                    skipFirstLine = false;
                    continue;
                } else {

                    String[] data = line.split(",");
                    saveToDB(data[1]);

                }

            }
        }
        br.close();

        return "Uploaded Successfully!!!";

    }

    void saveToDB(String name) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        em.createNativeQuery("INSERT INTO train_type (name) VALUES (?)")
                .setParameter(1, name)
                .executeUpdate();
        tx.commit();
        em.close();

    }



    private void saveUploadedFile(MultipartFile file) throws IOException {
        try {
            if (!file.isEmpty()) {
                byte[] bytes = file.getBytes();
                Path path = Paths.get(file.getOriginalFilename());
                Files.write(path, bytes);
            }
        } catch (Exception e) {

        }
    }
}
