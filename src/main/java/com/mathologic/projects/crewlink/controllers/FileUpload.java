package com.mathologic.projects.crewlink.controllers;

import com.mathologic.projects.crewlink.models.Trains;
import com.mathologic.projects.crewlink.repositories.TrainsRepository;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by santosh on 2/2/2022
 */
@Controller
public class FileUpload {


//    private final Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired(required=true)
    TrainsRepository trainsRepository;


    @RequestMapping(value="/upload", method= RequestMethod.GET)
    public @ResponseBody String provideUploadInfo() {
        return "You can upload a file by posting to this same URL.";
    }

    @RequestMapping(value="/uploadTrains", method= RequestMethod.POST)
    public @ResponseBody String uploadTrains(@RequestParam("file") MultipartFile TrainDetails,HttpServletRequest request) {

        if (TrainDetails.isEmpty()) {
            return "failed to upload " + TrainDetails + " because the file was empty.";
        }
        if (!TrainDetails.isEmpty()) {

            try{
                if(!TrainDetails.getOriginalFilename().contains(".csv")){
                    return "Uploaded file is not CSV";
                }


                String path = new File("/workspace_server_java_2022/crewlink/src/main/resources/").getAbsolutePath();


               String nameTrainDetails = path+"/uploads";

                File file = new File(nameTrainDetails);
                file.mkdirs();
                nameTrainDetails += "/"+TrainDetails.getOriginalFilename();

                file =  new File(nameTrainDetails);
                byte[] bytes = TrainDetails.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(file));
                stream.write(bytes);
                stream.close();
                processTrainRecords(nameTrainDetails);

            }catch(Exception e){
                System.out.println("Exception in uploadTrains()"+e.getMessage());
            }
        }

        return "Uploaded successfully !!!";
    }

  private void processTrainRecords(String nameTrainDetails){


        try {
            String[] nextLine = null;
            CSVReader reader = new CSVReaderBuilder(new FileReader(nameTrainDetails)).withSkipLines(1).build();

            while ((nextLine  = reader.readNext()) != null) {
                saveTrainRecords(Integer.parseInt(nextLine [0]),nextLine[1],nextLine[2],nextLine[3]);
            }



            reader.close();

        }catch (Exception e){
            System.out.println("Exception in ProcessRecords()"+e.getMessage());
            System.out.println("Exception in ProcessRecords()"+e.getStackTrace());

        }

  }

    private void saveTrainRecords(Integer trainNo,String train_name,String from_station,String to_station){

        try{
            Trains train = new Trains();
            train.setTrainNo(trainNo);
            train.setTrainName(train_name);
            train.setFromStation(from_station);
            train.setToStation(to_station);


//        train.setStartDay();
            trainsRepository.save(train);
            System.out.println("records in saveTrainRecords()");
        }catch(Exception e){
            System.out.println("Exception in saveTrainRecords()"+e.getMessage());
            System.out.println("Exception in saveTrainRecords()"+e.getStackTrace());
        }

    }


  String getAbsolutePaths(){
      Path currentRelativePath = Paths.get("");
      String s = currentRelativePath.toAbsolutePath().toString();
      String nameTrainDetails = s+"/uploads";
      return nameTrainDetails;
  }

}
